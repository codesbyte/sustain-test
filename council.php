<?php include("include/header.php");?>
<?php include("function.php");?>
<?php include("include/left.php"); 
$councilData =  getCouncil($cid);
//print_r($councilData);
if(isset($_POST['search']))
{
	if($_POST['search_address']!="")
	{
		$address = $_POST['search_address'];
		$councilData_array =  getCouncilByName($address);
		if($councilData_array)
		{
			$councilData = $councilData_array;	
		}
		else
		{
			$councilData = "";	
		}	
	}	
}
?>
<form method="post" action="" name="frm">
					<div class="col-xs-11 col-sm-9 col-md-9 mbl_search">
						<div class="col-xs-12 col-sm-12 col-md-12 main_search">							
						
							<div class="col-xs-12 col-sm-12 col-md-12 head_main">
								<div class="col-xs-12 col-sm-12 col-md-12 search_head food_waste">Council & Me</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 food_main">
								<div class="col-xs-12 col-sm-4 col-md-4 food_item sear_council">
									<div class="col-xs-12 col-sm-12 col-md-12 couc_head">Change your council</div>
									<div class="col-xs-12 col-sm-12 col-md-12">
										
											<input type="search" name="search_address" placeholder="Your address & suburb here" value="<?php if(isset($_POST['search_address'])){echo $_POST['search_address'];}else{echo $councilData['address'];}?>">
										
									</div>	
									<div class="col-xs-12 col-sm-12 col-md-12 coun_btn">
										<div class="col-xs-6 col-sm-6 col-md-6 mbl_search1">
											<input type="submit" class="search-btn" name="search" value="Search"/>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 mbl_search1">
											<a href="map.php?cid=<?php echo $cid;?>&aid=<?php echo $aid;?>" class="search-btn">Set location from map</a>
										</div>										
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 set_coun">
										<!--<a href="#" class="search-btn">Set Council</a>-->
                                        <input type="submit" class="search-btn" name="search" value="Set Council"/>
									</div>
								</div>
                                <?php
                                if($councilData)
								{
								?>
								<div class="col-xs-12 col-sm-8 col-md-8 food_desc">
									<div class="col-xs-12 col-sm-12 col-md-12 coffee moreland">
										<!--<img src="images/more.png" alt="food">-->
										 <div class="col-xs-12 col-sm-12 col-md-12 coffee_gr"><?php echo $councilData['name'];?></div> 
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 coffee_desc">
                                    
                                    
                                        <?php
		$feed = array();
		if($councilData['rssfeed'])
		{
    	$rss = new DOMDocument();
    	$rss->load($councilData['rssfeed']);
    	$feed = array();
		if($rss->getElementsByTagName('item'))
    	foreach ($rss->getElementsByTagName('item') as $node) {
    		$item = array ( 
    			'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
    			'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
    			'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
    			'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
    			);
    		array_push($feed, $item);
    	}
    	$limit = count($feed);
		}
    	
    ?>
										<div class="col-xs-12 col-sm-12 col-md-6 food_item coun_item">
											<ul>
                                            
                                            <?php
											if($feed)
                                            for($x=0;$x<$limit;$x++) {
												$title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
												$link = $feed[$x]['link'];
												$description = substr($feed[$x]['desc'],0,40)."....";
												//$date = date('l F d, Y', strtotime($feed[$x]['date']));
												//echo '<p><strong><a href="'.$link.'" title="'.$title.'">'.$title.'</a></strong><br />';
												//echo '<small><em>Posted on '.$date.'</em></small></p>';
												//echo '<p>'.$description.'</p>';
											
											?>
                                            
												<li>
													<a href="<?php echo $link;?>" title="<?php echo $title;?>">
														<div class="coun_1"><?php echo $title;?></div>
														<div class="coun_2"><?php echo $description;?></div>
														<span class="next"><img src="images/next.png" alt="next"></span>
													</a>
												</li>
                                                <?php
											}
												?>
												<!--<li>
													<a href="#">
														<div class="coun_1">A-Z guide to waste and recycling</div>
														<div class="coun_2">Look here to find out where you can recycle</div>
														<span class="next"><img src="images/next.png" alt="next"></span>
													</a>
												</li>
												<li>
													<a href="#">
														<div class="coun_1">Community Compost Hubs</div>
														<div class="coun_2">Community Compost Hubs - a service available</div>
														<span class="next"><img src="images/next.png" alt="next"></span>
													</a>
												</li>-->
											</ul>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 src_me food_pevt">
										<!--<a href="#" class="search-btn">Dob in a dump</a>-->
										<a href="#" class="search-btn">Address <?php echo $councilData['address'];?></a>
                                        <a href="#" class="search-btn">Phone <?php echo $councilData['phonenumber'];?></a>
									</div>	
								</div>
                                <?php
								}
								else
								{
								?>
                                <div class="col-xs-12 col-sm-8 col-md-8 food_desc">
                               
                                <div class="col-xs-12 col-sm-12 col-md-12 coffee moreland">
										<!--<img src="images/more.png" alt="food">-->
										 <div class="col-xs-12 col-sm-12 col-md-12 coffee_gr"> No Data Found.</div> 
									</div>
                                </div>
                                <?php
								}
								?>
							</div>

						</div>	
					</div>
                  </form>
				</div>				
			</div>	
		</div>
       
<?php include("include/footer.php");?>


