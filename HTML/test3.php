<script>

function getLocationFromAddress(address)
{
    $('#text-1').attr("placeholder", "Your address & suburb here");
    
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
   lat = results[0].geometry.location.lat();
   long = results[0].geometry.location.lng();
            
            //-----------------------------------------
            var element = document.getElementById('fterror');
            var btnCouncil = document.getElementById('setcouncilbutton');
            var councilName;
            var councilID;
   var areaID = 1;
            
            jQuery.getJSON("js/councilMap.json", function(data){
                councilJson = data;
                
                console.log(lat + " " + long);

                var councilBounds = [];
                var coordArray = councilJson.features;

                for (var i = 0; i < coordArray.length; i++) {
                    var extractedPoints = [];
                    var polygonPoints = coordArray[i].geometry.coordinates[0];

                    for (var j = 0; j < polygonPoints.length; j++) {
                        var splitCoords = (polygonPoints[j] + '').split(",");
                        extractedPoints.push(new google.maps.LatLng(splitCoords[1], splitCoords[0]));
                    }

                    //Add all the points together as a polygon
                    var councilPolygon = new google.maps.Polygon({
                        paths: extractedPoints
                      });

                    councilBounds.push(councilPolygon);
                }

                var location = new google.maps.LatLng(lat, long);

               for (var i = 0; i < councilBounds.length; i++) {
                    if (google.maps.geometry.poly.containsLocation(location, councilBounds[i])) {
                        councilName = councilJson.features[i].properties.name;
      
      //No area given
                        if (councilJson.features[i].properties.description.indexOf('-') != -1)
      {
       var splitDesc = councilJson.features[i].properties.description.split("-");
       councilID = parseInt(splitDesc[0]);
       areaID = parseInt(splitDesc[1]);
      }
      else
       councilID = parseInt(councilJson.features[i].properties.description);
       
      console.log(councilName + " " + councilID + " " + areaID);
      window.COUNCIL_ID = councilID;
      window.AREA_ID = areaID;
     }
                }

                if ((councilID !== undefined) && (councilName !== undefined)) {
                    //element.innerHTML = "Your address is managed by <b style='color: #C3D82F'>" + councilName + "</b>."
                    
                    element.innerHTML = ""
                    btnCouncil.innerHTML = "Use " + councilName;
                    btnCouncil.disabled = false;
                    btnCouncil.onclick = function(){saveCouncilInfoFirstTime(councilID, councilName);}
                    
                    //hideLocationSearchDialog();
                }
                else {
                    element.innerHTML = "Your council was not found, Sustain Me does not support your council at this time.";
                    btnCouncil.disabled = true;
                    btnCouncil.innerHTML = "Set Council";
                }

                geocoder = new google.maps.Geocoder();
                geocoder.geocode({'latLng': location}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                      if (results[0]) {
                        //console.log(results);
                        //console.log(results[0].address_components[5].long_name);
                       // saving a postcode temporarily
                       tempPostcode = results[0].address_components[5].long_name;
                      } 
                        else {
                        alert('No results found');
                      }
                    } else {
                        alert('Could not connect to the internet. Please recheck your connection and try again.');
                        element.innterHTML = "";
                        btnCouncil.disabled = true;
                        btnCouncil.innerHTML = "Set Council";
                    }
                });
            });
            //-----------------------------------------
        } 
        else {
            if (status === "ZERO_RESULTS")
            {
                alert('Please enter your address and try again.');
                element.innterHTML = "";
            }
          
        }
    });
}

</script>
