<script src="js/jquery.min.js"></script>
<!--<script type="text/javascript" src="https://developers.google.com/maps/documentation/javascript/error-messages#no-api-keys"></script>-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key= AIzaSyBXEC8EuE6r19hjoD7dcvkzaTdKQ-2K5gU &v=3.exp&signed_in=true&libraries=geometry"></script>

<script type="text/javascript">
    var currgeocoder;
 	var currentAddress;
    //Set geo location lat and long
    navigator.geolocation.getCurrentPosition(function (position, html5Error) {
        geo_loc = processGeolocationResult(position);
        currLatLong = geo_loc.split(",");
        initializeCurrent(currLatLong[0], currLatLong[1]);
    });

    //Get geo location result
    function processGeolocationResult(position) {
        html5Lat = position.coords.latitude; //Get latitude
        html5Lon = position.coords.longitude; //Get longitude
        html5TimeStamp = position.timestamp; //Get timestamp
        html5Accuracy = position.coords.accuracy; //Get accuracy in meters
        return (html5Lat).toFixed(8) + ", " + (html5Lon).toFixed(8);
    }

    //Check value is present or
    function initializeCurrent(latcurr, longcurr) {
        currgeocoder = new google.maps.Geocoder();

        console.log(latcurr + "-- ######## --" + longcurr);

        if (latcurr != '' && longcurr != '') {
            //call google api function
            var myLatlng = new google.maps.LatLng(latcurr, longcurr);
            return getCurrentAddress(myLatlng);
        }
    }

    //Get current address
    function getCurrentAddress(location) {
        currgeocoder.geocode({
            'location': location
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(results[0]);
                //$("#address").html(results[0].formatted_address);
				currentAddress = results[0].formatted_address;
				$("#address").val(currentAddress);
				//alert(results[0].formatted_address);
				currentAddress = "Ballarat City Council";
				get_council(currentAddress);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

//alert(currentAddress);
function get_council(currentAddress)
{
	//alert(currentAddress);
var geocoder = new google.maps.Geocoder();
var address = currentAddress;
geocoder.geocode({ 'address': address }, function (results, status) 
{
	if (status == google.maps.GeocoderStatus.OK) 
	{		
		var lat = results[0].geometry.location.lat();
		var long = results[0].geometry.location.lng();		
		var element = document.getElementById('fterror');
		var btnCouncil = document.getElementById('setcouncilbutton');
		var councilName;
		var councilID;
		var areaID = 1;	
		jQuery.getJSON("js/councilMap.json", function(data){
		councilJson = data;	
		var councilBounds = [];
		var coordArray = councilJson.features;
	
		for (var i = 0; i < coordArray.length; i++)
		{
			var extractedPoints = [];
			var polygonPoints = coordArray[i].geometry.coordinates[0];		
			for (var j = 0; j < polygonPoints.length; j++) 
			{
				var splitCoords = (polygonPoints[j] + '').split(",");
				extractedPoints.push(new google.maps.LatLng(splitCoords[1], splitCoords[0]));
			}		
			//Add all the points together as a polygon
			var councilPolygon = new google.maps.Polygon({
			paths: extractedPoints
			});			
			councilBounds.push(councilPolygon);
		}
	
	var location = new google.maps.LatLng(lat, long);	
	for (var i = 0; i < councilBounds.length; i++) 
		{
			if (google.maps.geometry.poly.containsLocation(location, councilBounds[i])) 
				{
					councilName = councilJson.features[i].properties.name;					
					//No area given
					if (councilJson.features[i].properties.description.indexOf('-') != -1)
					{
						var splitDesc = councilJson.features[i].properties.description.split("-");					
						councilID = parseInt(splitDesc[0]);
						areaID = parseInt(splitDesc[1]);
					}
					else
					{
						councilID = parseInt(councilJson.features[i].properties.description);
						console.log(councilName + " " + councilID + " " + areaID);
						//alert(councilName + " " + councilID + " " + areaID);
						//alert("Address: " + address + "\n\nLatitude: " + lat + "\n\nLongitude: " + long + "\n\n" + councilName + " " + councilID + " " + areaID);
						
						window.COUNCIL_ID = councilID;
						window.AREA_ID = areaID;
						$("#areaID").val(areaID);
						$("#councilID").val(councilID);
					}
				}
		}
	});
	//-----------------------------------------
	} 
	else 
	{
		if (status === "ZERO_RESULTS")
		{
			//alert('Please enter your address and try again.');
			element.innterHTML = "";
		}
	}
});
}
       // };
        //-->
    </script>
<input type="hidden" name="address" value="" id="address">
<input type="hidden" name="address" value="" id="councilID">
<input type="hidden" name="address" value="" id="areaID">
<div id="divId"></div>