<?php include("include/header.php");?>
<?php include("function.php");?>
<?php include("include/left.php");
$search = "";
if(isset($_POST['search']))
{
	$search =	$_POST['search_txt'];
}
//print_r($_POST);
?>        

<div class="col-xs-11 col-sm-9 col-md-9 mbl_search">
    <div class="col-xs-12 col-sm-12 col-md-12 main_search">							
    
        <div class="col-xs-12 col-sm-12 col-md-12 head_main">
            <div class="col-xs-12 col-sm-12 col-md-12 search_head">What would you like to recycle?</div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 searching">
        <form method="post" action="items_serch.php?cid=<?php echo $cid;?>&aid=<?php echo $aid;?>">
                <input  type="search" value="<?php echo $search ;?>" id="search" name="search_txt" placeholder="Search for an item">
                <input type="submit"  name="search" value="" />
           </form>
            <div class="col-xs-12 col-sm-12 col-md-12 search_or">
                <!--or-->
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 search_item" align="center" >
            <div class="col-xs-12 col-sm-12 col-md-12 select_item">
                <!--Select item from categories below-->
            </div>
            
            <div id="divId"></div>
            
        </div>
        
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 search_item">
            <div class="col-xs-12 col-sm-12 col-md-12 select_item">
                Select item from categories below
            </div>
            <?php 
            $getAllCategory = getAllCategory($search);
            foreach($getAllCategory as $key)
            { 
            ?>	
            <div class="col-xs-12 col-sm-6 col-md-6 outer-box">
                <a href="category_items.php?catid=<?php echo $key['id'];?>&cid=<?php echo $cid;?>&aid=<?php echo $aid;?>" class="<?php echo $key['class'];?>"><?php echo $key['name'];?></a>
            </div>
            <?php 
            } ?>						
        </div>

    </div>	
</div>
</div>				
</div>	
</div>  <!--  content section close -->
<?php include("include/footer.php");?>