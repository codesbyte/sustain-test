<?php include("include/header.php");?>
		<div class="content"> <!--  content section -->
			<div class="container header-cont">
				<div class="col-xs-12 col-sm-12 col-md-12 main-content">
					<div class="col-xs-10 col-sm-3 col-md-3 home-screen">
						<div class="col-xs-12 col-sm-12 col-md-12 logo">
							<a href="#">
								<img src="images/logo1.png" alt="logo image" class="logo_image">
								<img src="images/title.svg" alt="title image" class="title_image">
							</a>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 main-que">
							What do you want to do?
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 src_me">
							<a href="#" class="search-btn">Search me</a>
							<a href="#" class="search-btn">Remind me</a>
							<a href="#" class="search-btn">Council & me</a>
						</div>						
					</div>

					<div class="col-xs-11 col-sm-9 col-md-9 mbl_search">
						<div class="col-xs-12 col-sm-12 col-md-12 main_search">							
						
							<div class="col-xs-12 col-sm-12 col-md-12 head_main">
								<div class="col-xs-12 col-sm-12 col-md-12 search_head">What would you like to recycle?</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 searching">
								<form name="frm_search" id="frm_search">
									<input type="search" name="search" placeholder="Search for an item">
									<input type="submit" name="submit" value="">
								</form>
								<div class="col-xs-12 col-sm-12 col-md-12 search_or">
									or
								</div>
							</div>

							<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 search_item">
								<div class="col-xs-12 col-sm-12 col-md-12 select_item">
									Select item from categories below
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="food">food waste</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="gwaste">green-waste</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="paper">paper</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="plastic">plastic</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="alus">aluminium and steel</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="glass">glass</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="car">car accessories</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="whiteg">whitegoods</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="elec">electronics</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="furn">furniture</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="cloth">clothes</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="house">household items</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 outer-box">
									<a href="#" class="const">construction material</a>
								</div>								
							</div>
						</div>	
					</div>
				</div>				
			</div>	
		</div>  <!--  content section close -->
<?php include("include/footer.php");?>