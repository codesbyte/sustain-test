<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="js/jquery.min.js"></script>

	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	  	<script src="js/bootstrap.min.js" ></script>

	  	<!--Calender-->	
			<script src="calender_js/underscore-min.js"></script>
			<script src="calender_js/moment-2.2.1.js"></script>
			<!--<script src="calender_js/clndr.js"></script>-->
			
		<!--End Calender-->
		<!-- <script type="text/javascript" src="js/jquery.e-calendar.js"></script>
    	<script type="text/javascript" src="js/index.js"></script> -->

	  	<!-- toggle navigation -->
		<script type="text/javascript">
			$(document).ready(function(){
				$(".toggle-nav").click(function(){
					$(".navbar-collapse").toggle("slide");
				});
			});
		</script>		

	</body>	<!-- body section close-->
</html>