<?php
if(!isset($_GET['cid']) || !isset($_GET['aid']))
{
	header('Location: index.php');
}
else
{
	$cid = $_GET['cid'];
	$aid = $_GET['aid'];
}
?>
<script>
function clickme(page)
{
	var areaID ='<?php echo  $aid;?>';
	var councilID = '<?php echo  $cid;?>';
	window.location.href=''+page+'?cid='+councilID+'&aid='+areaID;
}
</script>
<div class="content"> <!--  content section -->
			<div class="container header-cont">
				<div class="col-xs-12 col-sm-12 col-md-12 main-content">
					<div class="col-xs-10 col-sm-3 col-md-3 home-screen search_screen">
						<div class="col-xs-12 col-sm-12 col-md-12 logo">
							<a href="#">
								<img src="images/logo1.png" alt="logo image" class="logo_image">
								<img src="images/title.svg" alt="title image" class="title_image">
							</a>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 main-que">
							What do you want to do?
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 src_me">
							<a href="javascript:void(0)" onClick="clickme('search.php');" class="search-btn">Search me</a>
							<a href="javascript:void(0)" onClick="clickme('remind.php');" class="search-btn">Remind me</a>
							<a href="javascript:void(0)" onClick="clickme('council.php');" class="search-btn">Council & me</a>
						</div>						
					</div>   