<?php
include("inner_header.php");
include("left.php");
?>



<div class="col-xs-11 col-sm-9 col-md-9 mbl_search">
<div class="col-xs-12 col-sm-12 col-md-12 main_search item_search">							

    <div class="col-xs-12 col-sm-12 col-md-12 head_main">
        <div class="col-xs-12 col-sm-12 col-md-12 search_head item_head">Categories</div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 searching f_searching ui-content"  id="main-content">
        <form>
            <div class="col-xs-12 col-sm-12 col-md-12 item_api">
                <input type="text" name="api_key" placeholder="Enter your API Key here" class="api_key apitextbox">
            </div>	
            <div class="col-xs-12 col-sm-12 col-md-12 coffee_desc create_item">Create Category</div>
            <div class="col-xs-12 col-sm-12 col-md-12 item_api">
                <input id="category-name" type="text" name="api_key" placeholder="Please enter category name" class="api_key">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 item_sub">
            	<p id="category-result"></p>
                <button type="button" name="submit" id="category-submit-button" class="search-btn">Submit</button>
            </div>
        </form>
    </div>

</div>	
</div>
</div>				
</div>	
</div>
<script>
	$("#category-submit-button").on('submit',function(){console.log("ENTER PRESSED"); return false;});

	$("#category-submit-button").on('vclick',function(){
		console.log("CATEGORY BUTTON PRESSED");
		
		var $form = $(this);
		var $inputs = $form.find("input, select, button, textarea, text");
		$inputs.prop("disabled", true);
		
		var nameData = $("#category-name").val();
		//var catData = 'a2873d6a5e9734f5c2dc9232bcaefc03';
		var catData = $(".api_key").val();
		
		//console.log(readCookie('apiKey'));
		//console.log(infoData);
		
		$.ajax({
			type: "POST",
			url: "http://sustainmeapp.com/api/v1/category",
			data: {
				name: nameData
			},
			beforeSend: function (xhr) {
				xhr.setRequestHeader ("Authorization", catData);
			},
			success: function(response) {
				$("#category-result").text("SUCCESS: " + response.message);
				$("#category-result").css( "color", "green" );
				reloadCategories();
			},
			error: function(response) {
				$("#category-result").text("ERROR: " + response.responseJSON.message);
				$("#category-result").css( "color", "red" );
			},
			complete: function() {
				$inputs.prop("disabled", false);
			}
		});
	});
</script>

<!--<div role="main" class="ui-content" id="main-content">
				<label for="text-1">API Key:</label>
				<input class="apitextbox" type="text" placeholder="Enter your API key here..." data-clear-btn="true">
		
				<hr>
		
				<h2>Create Category</h2>
			
				<form id="create-category">
					<label for="text-1">Name:</label>
					<input id="category-name" type="text" data-clear-btn="true">
					
					<p id="category-result"></p>
					<input type="button" id="category-submit-button" value="Submit">
				</form>
	<?php //include("footer.php");?>			
				<script>
					$("#category-submit-button").on('submit',function(){console.log("ENTER PRESSED"); return false;});
				
					$("#category-submit-button").on('vclick',function(){
						console.log("CATEGORY BUTTON PRESSED");
						
						var $form = $(this);
						var $inputs = $form.find("input, select, button, textarea, text");
						$inputs.prop("disabled", true);
						
						var nameData = $("#category-name").val();
						
						//console.log(readCookie('apiKey'));
						//console.log(infoData);
						
						$.ajax({
							type: "POST",
							url: "http://sustainmeapp.com/api/v1/category",
							data: {
								name: nameData
							},
							beforeSend: function (xhr) {
								xhr.setRequestHeader ("Authorization", 'a2873d6a5e9734f5c2dc9232bcaefc03');
							},
							success: function(response) {
								$("#category-result").text("SUCCESS: " + response.message);
								$("#category-result").css( "color", "green" );
								reloadCategories();
							},
							error: function(response) {
								$("#category-result").text("ERROR: " + response.responseJSON.message);
								$("#category-result").css( "color", "red" );
							},
							complete: function() {
								$inputs.prop("disabled", false);
							}
						});
					});
				</script>
		</div>--><!-- /content -->