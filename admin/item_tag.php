<?php
include("inner_header.php");
include("left.php");
?>



<div class="col-xs-11 col-sm-9 col-md-9 mbl_search">
						<div class="col-xs-12 col-sm-12 col-md-12 main_search item_search">							
						
							<div class="col-xs-12 col-sm-12 col-md-12 head_main">
								<div class="col-xs-12 col-sm-12 col-md-12 search_head item_head">Item Tags</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 searching f_searching">
								<form method="post">
									<div class="col-xs-12 col-sm-12 col-md-12 item_api">
										 <input type="text" name="api_key" placeholder="Enter your API Key here" class="api_key apitextbox">
									</div>	
									<div class="col-xs-12 col-sm-12 col-md-12 coffee_desc create_item">Create Item Tag</div>
									<div class="col-xs-12 col-sm-12 col-md-12 item_api">
										<input type="text" id="item-tag-info" name="item-tag-info" placeholder="Please enter information" class="api_key">
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 item_sub">
                                    <p id="itemtag-result"></p>
										<input type="button" id="itemtag-submit-button" name="submit" value="Submit" class="search-btn">
									</div>
								</form>
							</div>

						</div>	
					</div>
</div>				
</div>	
</div>
<script>
$("#itemtag-submit-button").on('submit',function(){console.log("ENTER PRESSED"); return false;});
				
					$("#itemtag-submit-button").on('vclick',function(){
						console.log("ITEM TAG BUTTON PRESSED");
						
						var $form = $(this);
						var $inputs = $form.find("input, select, button, textarea, text");
						$inputs.prop("disabled", true);
						
						var infoData = $("#item-tag-info").val();
						var apiData = $(".api_key").val();
						
						//console.log(readCookie('apiKey'));
						//console.log(infoData);
						
						$.ajax({
							type: "POST",
							url: "http://sustainmeapp.com/api/v1/item_tag",
							data: {
								information: infoData
							},
							beforeSend: function (xhr) {
								xhr.setRequestHeader ("Authorization", apiData);
							},
							success: function(response) {
								$("#itemtag-result").text("SUCCESS: " + response.message);
								$("#itemtag-result").css( "color", "green" );
								reloadItemTags();
							},
							error: function(response) {
								$("#itemtag-result").text("ERROR: " + response.responseJSON.message);
								$("#itemtag-result").css( "color", "red" );
							},
							complete: function() {
								$inputs.prop("disabled", false);
							}
						});
					});
				</script>

