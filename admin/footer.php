<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="js/jquery.min.js"></script>

	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	  	<script src="js/bootstrap.min.js" ></script>

	  	<!-- toggle navigation -->
		<script type="text/javascript">
			$(document).ready(function(){
				$(".toggle-nav").click(function(){
					$(".navbar-collapse").toggle("slide");
				});
			});
		</script>		

	</body>	<!-- body section close-->
</html>