<?php include("header.php");
include("function.php");

if(isset($_POST['submit']))
{	
	$username = $_POST['username'];
	$password = $_POST['password'];
	$result = adminLogin($username,$password);
	if($result)
	{		
		$_SESSION['admin_id'] = $result['id'];
		$_SESSION['admin_name'] = $result['name'];
		$_SESSION['admin_email'] = $result['email'];
		redirect('select.php');	
	}
}
if(isset($_SESSION['admin_id']) && $_SESSION['admin_id']!="")
{
	redirect('select.php');	
}
?>
<div class="content"> <!--  content section -->
    <div class="container header-cont">
        <div class="col-xs-12 col-sm-12 col-md-12 main-content">
            <div class="home-screen home_form welcome_screen">
                <div class="col-xs-12 col-sm-12 col-md-12 logo">
                    <a href="#">
                        <img src="images/logo1.png" alt="logo image" class="logo_image">
                        <img src="images/title.svg" alt="title image" class="title_image">
                    </a>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 main-que">
                    Welcome!
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 src_me">
                    <form method="post" action="">
                        <div class="form-group">
                            <input type="text" name="username" value="" placeholder="Enter your Username" class="wel_input">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" value="" placeholder="Password" class="wel_input">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 wel_login">
                            <input type="submit" name="submit" value="Login" class="search-btn">
                        </div>
                    </form>
                    <div class="col-xs-12 col-sm-12 col-md-12 forgate_pass">
                        <!--<a href="forgot_password.php">Forgot your password?</a>-->
                    </div>
                </div>						
            </div>
        </div>				
    </div>	
</div>  <!--  content section close -->
<?php include("footer.php");?>