<?php
include("inner_header.php");
include("left.php");
?>
<style type="text/css">
	.Switch {
		position: relative;
		display: inline-block;
		font-size: 14px;
		color: #ccc;
		height: 42px;
		border: 2px solid #BED83F;
		border-radius: 50px;
		cursor: pointer;
		padding: 5px;
	}

.Switch span { display: inline-block; width: 40px; position: relative;top: 4px;}
.Switch span.On { color: #BED83F; }

.Switch .Toggle {
position: absolute;
top: 4px;
width: 30px;
height: 30px;
border-radius: 50px;
background: #BED83F;
z-index: 999;
-webkit-transition: all 0.15s ease-in-out;
-moz-transition: all 0.15s ease-in-out;
-o-transition: all 0.15s ease-in-out;
-ms-transition: all 0.15s ease-in-out;
}

.Switch.On .Toggle { left: 10%; }
.Switch.Off .Toggle { left: 58%; }

</style>

<div class="col-xs-11 col-sm-9 col-md-9 mbl_search">
    <div class="col-xs-12 col-sm-12 col-md-12 main_search item_search">							
    
        <div class="col-xs-12 col-sm-12 col-md-12 head_main">
            <div class="col-xs-12 col-sm-12 col-md-12 search_head item_head">Items</div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 searching f_searching">
            <form method="post">
                <div class="col-xs-12 col-sm-12 col-md-12">
                   <input type="text" name="api_key" placeholder="Enter your API Key here" class="api_key apitextbox">
                </div>	
                <div class="col-xs-12 col-sm-12 col-md-12 coffee_desc create_item">Create Item</div>
                <div class="col-xs-12 col-sm-12 col-md-6 item_select">
                    <div class="item_label">Council ID: </div>
                    <div class="item_field">
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                        <select class="cd-select" id="item-council">
                            <option value="0">Select from drop down menu</option>                            
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 item_select">
                    <div class="item_label">Item tag ID: </div>
                    <div class="item_field">
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                        <select class="cd-select" id="item-item-tag">
                            <option value="0">Select from drop down menu</option>                           
                        </select>
                    </div>
                </div>	
                <div class="col-xs-12 col-sm-12 col-md-6 item_select">
                    <div class="item_label">Category ID: </div>
                    <div class="item_field">
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                        <select class="cd-select" id="item-category">
                            <option value="0">Select from drop down menu</option>                           
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 item_select">
                    <div class="item_label">Business ID: </div>
                    <div class="item_field">
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                        <select class="cd-select" id="item-business">
                            <option value="0" selected="selected">Select from drop down menu</option>
                          
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 item_select">
                    <div class="item_label">Name: </div>
                    <div class="item_field">
                        <input type="text" name="item_name" id="item-name" placeholder="Enter Item Name" class="api_key">
                    </div>
                </div>	
                <div class="col-xs-12 col-sm-12 col-md-6 item_select">
                    <div class="switch_label">Is this a donation entry:</div>
                    <!-- <div class="onoffswitch">
                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                        <label class="onoffswitch-label" for="myonoffswitch">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div> -->
                    <div class="Switch Off">
						<div class="Toggle" id="myonoffswitch"></div>
						<span class="On">ON</span>
						<span class="Off">OFF</span>
					</div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="text" name="api_key" id="item-info" placeholder="Please enter any addition information" class="api_key">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 item_sub">
                <p id="item-result"></p>
                    <button type="button" name="submit" id="item-submit-button" class="search-btn">Submit</button>
                </div>
            </form>
        </div>

    </div>	
</div>
</div>				
</div>	
</div>
<script>

reloadCouncils();
reloadCategories();
reloadItemTags();
reloadBusinesses();



//COUNCILS
		function reloadCouncils() {
			$.ajax({
				type: "GET",
				url: "http://sustainmeapp.com/api/v1/council",
				contentType: "application/json",
				dataType: "json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", 'a2873d6a5e9734f5c2dc9232bcaefc03');
				},
				success: function(response) {
					console.log(response);
					var html = '';
					var len = response.councils.length;
					
					$('#item-council').find('option').remove().end();
					html+='<option disabled selected value=\'0\'> -- select a council -- </option>';
					
					for (var i = 0; i< len; i++) {
						html += '<option value="' + response.councils[i].id + '">' + response.councils[i].name + '</option>';
					}
					
					$("#item-council").append(html);
					$("#item-council").selectmenu("refresh");
				},
				error: function(response) {
					console.log(response);
				}
			});
		}
		
		//CATEGORIES
		function reloadCategories() {
			$.ajax({
				type: "GET",
				url: "http://sustainmeapp.com/api/v1/category",
				contentType: "application/json",
				dataType: "json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", 'a2873d6a5e9734f5c2dc9232bcaefc03');
				},
				success: function(response) {
					console.log(response);
					var html = '';
					var len = response.categories.length;
					
					$('#item-category').find('option').remove().end();
					html+='<option disabled selected value=\'0\'> -- select a category -- </option>';
					
					for (var i = 0; i< len; i++) {
						html += '<option value="' + response.categories[i].id + '">' + response.categories[i].name + '</option>';
					}
					
					$("#item-category").append(html);
					$("#item-category").selectmenu("refresh");
				},
				error: function(response) {
					console.log(response);
				}
			});
		}
		
		//ITEM TAGS
		function reloadItemTags() {
			$.ajax({
				type: "GET",
				url: "http://sustainmeapp.com/api/v1/item_tag",
				contentType: "application/json",
				dataType: "json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", 'a2873d6a5e9734f5c2dc9232bcaefc03');
				},
				success: function(response) {
					console.log(response);
					var html = '';
					var len = response.item_tags.length;
					
					$('#item-item-tag').find('option').remove().end();
					html+='<option disabled selected value=\'0\'> -- select an item tag -- </option>';
					
					for (var i = 0; i< len; i++) {
						html += '<option value="' + response.item_tags[i].id + '">' + response.item_tags[i].information + '</option>';
					}
					
					$("#item-item-tag").append(html);
					$("#item-item-tag").selectmenu("refresh");
				},
				error: function(response) {
					console.log(response);
				}
			});
		}
		
		//BUSINESSES
		function reloadBusinesses() {
			$.ajax({
				type: "GET",
				url: "http://sustainmeapp.com/api/v1/business",
				contentType: "application/json",
				dataType: "json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", 'a2873d6a5e9734f5c2dc9232bcaefc03');
				},
				success: function(response) {
					console.log(response);
					var html = '';
					var len = response.businesses.length;
					
					$('#item-business').find('option').remove().end();
					html+='<option disabled selected value=\'0\'> -- select a business -- </option>';
					
					for (var i = 0; i< len; i++) {
						html += '<option value="' + response.businesses[i].id + '">' + response.businesses[i].name + '</option>';
					}
					
					$("#item-business").append(html);
					$("#item-business").selectmenu("refresh");
				},
				error: function(response) {
					console.log(response);
				}
			});
		}


$("#item-submit-button").on('submit',function(){console.log("ENTER PRESSED"); return false;});	
		$("#item-submit-button").on('vclick',function(){
			console.log("ITEM BUTTON PRESSED");
			
			var $form = $(this);
			var $inputs = $form.find("input, select, button, textarea, text");
			$inputs.prop("disabled", true);
			
			var councilData = $("#item-council").val();
			var categoryData = $("#item-category").val();
			var nameData = $("#item-name").val();
			var donateData = $("#myonoffswitch").val();
			var infoData = $("#item-info").val();
			var itemTagData = $("#item-item-tag").val();
			var businessData = $("#item-business").val();
			var catData = $(".api_key").val();
			console.log(councilData + " " + categoryData + " " + nameData + " " + donateData + " " + infoData + " " + itemTagData + " " + businessData);
			
			//console.log(readCookie('apiKey'));
			console.log(infoData);
			
			$.ajax({
				type: "POST",
				url: "http://sustainmeapp.com/api/v1/item",
				data: {
					council_id: councilData,
					category_id: categoryData,
					name: nameData,
is_donate : donateData,
					information: infoData,
					tag_id: itemTagData,
					business_id: businessData
				},
				beforeSend: function (xhr) {
					xhr.setRequestHeader ("Authorization", catData);
				},
				success: function(response) {
					$("#item-result").text("SUCCESS: " + response.message);
					$("#item-result").css( "color", "green" );
				},
				error: function(response) {
					$("#item-result").text("ERROR: " + response.responseJSON.message);
					$("#item-result").css( "color", "red" );
				},
				complete: function() {
					$inputs.prop("disabled", false);
				}
			});
		});

$(document).ready(function() {
	
	// Switch toggle
	$('.Switch').click(function() {
		$(this).toggleClass('On').toggleClass('Off');
	});
	
});
</script>


