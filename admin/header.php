<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head><!-- html head section-->

		<!-- meta tags -->
		<meta charset="UTF-8">
		<meta name="description" content="This is an example of a meta description">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript,Jquery">
		<meta name="author" content="E-Commerce Website">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
	    <link rel="icon" href="images/logo.png" type="images/png" sizes="16x16">  
	    
		<!-- title -->
		<title>Sustain Me ~ Welcome</title>

	    <!-- bootstrap files -->
		<link href="css/bootstrap.min.css" rel="stylesheet">	

		<!-- external css -->
		<link href="css/style.css" rel="stylesheet" type="text/css"> 
		<link href="css/normalize.css" rel="stylesheet" type="text/css">	

		<!-- font file -->
		<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700" rel="stylesheet">
      

	</head><!-- html head section close -->
	<body><!-- body section -->	

		<div class="header">  <!-- header start -->
			<!-- header container -->
			<div class="container header-cont">
				<div class="col-xs-12 col-sm-12 col-md-12 main-head">					
					<nav class="navbar navbar-default">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <a class="toggle-nav" href="#"><img src="images/menu.png" alt="toggle"></a>
					      <a class="navbar-brand" href="index.html"><img src="images/logo1.png" alt="logo image"></a>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="navbar-collapse" >
					      <ul class="nav navbar-nav">
					        <li><a href="#">home</a></li>
					        <li><a href="#">about</a></li>
					        <li><a href="#">partners and allies</a></li>
					        <li><a href="#">crowdfunder</a></li>
					        <li><a href="#">social</a></li>
					        <li><a href="#">recycle with sustain me</a></li>
					        <li><a href="#">live sustainably blog</a></li>
					        <li><button class="btn download-btn">download sustain me</button></li>
					      </ul>
					    </div><!-- /.navbar-collapse -->
					</nav>
				</div>	
			</div>
			<!-- header container close -->
		</div>  <!-- header close -->
<?php
include('../config/config.php');
?>