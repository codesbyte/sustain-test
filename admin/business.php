<?php
include("inner_header.php");
include("left.php");
?>
<div class="col-xs-11 col-sm-9 col-md-9 mbl_search">
    <div class="col-xs-12 col-sm-12 col-md-12 main_search item_search">							
    
        <div class="col-xs-12 col-sm-12 col-md-12 head_main">
            <div class="col-xs-12 col-sm-12 col-md-12 search_head item_head">Business</div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 searching f_searching">
            <form class="business" method="post">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="text" name="api_key" placeholder="Enter your API Key here" class="api_key apitextbox">
                </div>	
                <div class="col-xs-12 col-sm-12 col-md-12 coffee_desc create_item">Create Business</div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 item_select council_select">
                    <div class="item_label council_label">Name: </div>
                    <div class="item_field council_field">
                        <input type="text" name="item_name" id="business-name" placeholder="" class="api_key">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 item_select council_select">
                    <div class="item_label council_label">URL: </div>
                    <div class="item_field council_field">
                        <input type="text" name="item_name" id="business-url" placeholder="" class="api_key">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 item_select council_select">
                    <div class="item_label council_label">Address: </div>
                    <div class="item_field council_field">
                        <input type="text" name="item_name" id="business-address" placeholder="" class="api_key">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 item_select council_select">
                    <div class="item_label council_label">Phone Number: </div>
                    <div class="item_field council_field">
                        <input type="text" name="item_name" id="business-number" placeholder="" class="api_key">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 item_sub">
                <p id="business-result"></p>
                    <button type="button" name="submit" id="business-submit-button" class="search-btn">Submit</button>
                </div>
            </form>
        </div>

    </div>	
</div>
</div>				
</div>	
</div>
<script>
$("#business-submit-button").on('submit',function(){console.log("ENTER PRESSED"); return false;});
				
					$("#business-submit-button").on('vclick',function(){
						console.log("BUSINESS BUTTON PRESSED");
						
						var $form = $(this);
						var $inputs = $form.find("input, select, button, textarea, text");
						$inputs.prop("disabled", true);
						
						var nameData = $("#business-name").val();
						var urlData = $("#business-url").val();
						var addressData = $("#business-address").val();
						var numberData = $("#business-number").val();
						var apiData = $(".api_key").val();
						//console.log(readCookie('apiKey'));
						//console.log(infoData);
						
						$.ajax({
							type: "POST",
							url: "http://sustainmeapp.com/api/v1/business",
							data: {
								name: nameData,
								url: urlData,
								address: addressData,
phone_number : numberData
							},
							beforeSend: function (xhr) {
								xhr.setRequestHeader ("Authorization", apiData);
							},
							success: function(response) {
								$("#business-result").text("SUCCESS: " + response.message);
								$("#business-result").css( "color", "green" );
								reloadBusinesses();
							},
							error: function(response) {
								$("#business-result").text("ERROR: " + response.responseJSON.message);
								$("#business-result").css( "color", "red" );
							},
							complete: function() {
								$inputs.prop("disabled", false);
							}
						});
					});
</script>

