<?php
include("header.php");

?>

<div class="content"> <!--  content section -->
			<div class="container header-cont">
				<div class="col-xs-12 col-sm-12 col-md-12 main-content">
					<div class="home-screen home_form">
						<div class="col-xs-12 col-sm-12 col-md-12 logo">
							<a href="#">
								<img src="images/logo1.png" alt="logo image" class="logo_image">
								<img src="images/title.svg" alt="title image" class="title_image">
							</a>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 main-que">
							Please select from below to start
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 src_me">
							<a href="items.php" class="search-btn">Items</a>
							<a href="item_tag.php" class="search-btn">Item Tags</a>
							<a href="council.php" class="search-btn">Councils</a>
							<a href="categories.php" class="search-btn">Categories</a>
							<a href="business.php" class="search-btn">Business</a>
                            <a href="logout.php"  class="search-btn">Logout</a>
						</div>						
					</div>
				</div>				
			</div>	
		</div>
  <?php
include("footer.php");
?>