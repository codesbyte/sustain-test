<?php
include("inner_header.php");
include("left.php");
?>



<div class="col-xs-11 col-sm-9 col-md-9 mbl_search">
<div class="col-xs-12 col-sm-12 col-md-12 main_search item_search">							

<div class="col-xs-12 col-sm-12 col-md-12 head_main">
    <div class="col-xs-12 col-sm-12 col-md-12 search_head item_head">Councils</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 searching f_searching">
    <form method="post">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <input type="text" name="api_key" placeholder="Enter your API Key here" class="api_key apitextbox">
        </div>	
        <div class="col-xs-12 col-sm-12 col-md-12 coffee_desc create_item">Create Council</div>
        
        <div class="col-xs-12 col-sm-12 col-md-6 item_select council_select">
            <div class="item_label council_label">Name: </div>
            <div class="item_field council_field">
                <input type="text" name="item_name" id="council-name" placeholder="" class="api_key">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 item_select council_select">
            <div class="item_label council_label">URL: </div>
            <div class="item_field council_field">
                <input type="text" name="item_name" id="council-url" placeholder="" class="api_key">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 item_select council_select">
            <div class="item_label council_label">Address: </div>
            <div class="item_field council_field">
                <input type="text" name="item_name" id="council-address" placeholder="" class="api_key">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 item_select council_select">
            <div class="item_label council_label">Phone Number: </div>
            <div class="item_field council_field">
                <input type="text" name="item_name"  id="council-number" placeholder="" class="api_key">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 item_select council_select">
            <div class="item_label council_label">Opening Hours: </div>
            <div class="item_field council_field">
                <input type="text" name="item_name" id="council-hours" placeholder="" class="api_key">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 item_select council_select">
            <div class="item_label council_label">RSS Feed: </div>
            <div class="item_field council_field">
                <input type="text" name="item_name" id="council-rss" placeholder="" class="api_key">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 item_sub">
        
        <p id="council-result"></p>
           <!-- <input type="submit" name="submit" id="council-submit-button" value="Submit" class="search-btn">-->
            
             <button type="button" name="submit" id="council-submit-button" class="search-btn">Submit</button>
        </div>
    </form>
</div>

</div>	
</div>
</div>				
</div>	
</div>
<script>
	$("#council-submit-button").on('submit',function(){console.log("ENTER PRESSED"); return false;});				
$("#council-submit-button").on('vclick',function(){
	console.log("COUNCIL BUTTON PRESSED");
	
	var $form = $(this);
	var $inputs = $form.find("input, select, button, textarea, text");
	$inputs.prop("disabled", true);
	
	var nameData = $("#council-name").val();
	var addressData = $("#council-address").val();
	var hoursData = $("#council-hours").val();
	var urlData = $("#council-url").val();
	var numberData = $("#council-number").val();
	var rssData = $("#council-rss").val();
	var catData = $(".api_key").val();
	//console.log(readCookie('apiKey'));
	//console.log(infoData);
	
	$.ajax({
		type: "POST",
		url: "http://sustainmeapp.com/api/v1/council",
		data: {
			name: nameData,
			address: addressData,
			openinghours: hoursData,
			url: urlData,
			phonenumber: numberData,
			rssfeed: rssData
		},
		beforeSend: function (xhr) {
			xhr.setRequestHeader ("Authorization", catData);
		},
		success: function(response) {
			$("#council-result").text("SUCCESS: " + response.message);
			$("#council-result").css( "color", "green" );
			reloadCouncils();
		},
		error: function(response) {
			$("#council-result").text("ERROR: " + response.responseJSON.message);
			$("#council-result").css( "color", "red" );
		},
		complete: function() {
			$inputs.prop("disabled", false);
		}
	});
});
</script>

