<?php
ob_start();
?>
<!DOCTYPE html>
<html>
	<head><!-- html head section-->

		<!-- meta tags -->
		<meta charset="UTF-8">
		<meta name="description" content="This is an example of a meta description">
		<meta name="keywords" content="HTML,CSS,XML,JavaScript,Jquery">
		<meta name="author" content="E-Commerce Website">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
	    <link rel="icon" href="images/logo.png" type="images/png" sizes="16x16">  
	    
		<!-- title -->
		<title>Sustain Me ~ Home</title>

	    <!-- bootstrap files -->
		<link href="css/bootstrap.min.css" rel="stylesheet">	

		<!-- external css -->
		<link href="css/style.css" rel="stylesheet" type="text/css"> 
       <link rel="stylesheet" href="calender_css/clndr.css" type="text/css" />
		<link href="calender_css/style.css" rel='stylesheet' type='text/css' />
		<link href="css/normalize.css" rel="stylesheet" type="text/css">	

		<!-- font file -->
		<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700" rel="stylesheet">

   		<!-- js file -->
        <script src="js/jquery.min.js"></script>
       <!-- <script type="text/javascript" src="https://developers.google.com/maps/documentation/javascript/error-messages#no-api-keys"></script>-->
        <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key= AIzaSyBXEC8EuE6r19hjoD7dcvkzaTdKQ-2K5gU&v=3.exp&signed_in=true&libraries=geometry"></script>-->
        <!--<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=geometry,places&amp;ext=.js" title="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=geometry,places&amp;ext=.js">
           </script>-->
   		<!-- js file -->
        
	</head><!-- html head section close -->
	<?php include("config/config.php");?>
    <body>
    	<div class="header">  <!-- header start -->
			<!-- header container -->
			<div class="container header-cont">
				<div class="col-xs-12 col-sm-12 col-md-12 main-head">					
					<nav class="navbar navbar-default">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <a class="toggle-nav" href="#"><img src="images/menu.png" alt="toggle"></a>
					      <a class="navbar-brand" href="index.html"><img src="images/logo1.png" alt="logo image"></a>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="navbar-collapse" >
					      <ul class="nav navbar-nav">
					        <li><a href="#">home</a></li>
					        <li><a href="#">about</a></li>
					        <li><a href="#">partners and allies</a></li>
					        <li><a href="#">crowdfunder</a></li>
					        <li><a href="#">social</a></li>
					        <li><a href="#">recycle with sustain me</a></li>
					        <li><a href="#">live sustainably blog</a></li>
					        <li><button class="btn download-btn">download sustain me</button></li>
					      </ul>
					    </div><!-- /.navbar-collapse -->
					</nav>
				</div>	
			</div>
			<!-- header container close -->
		</div>  <!-- header close -->
		<!-- temprory header -->
        
        <!-- temprory header -->
			<div class="col-x-12 col-sm-12 col-md-12 temp_main">
				<div class="container header-cont">
				 	<div class="col-x-12 col-sm-12 col-md-12 temp_header">
						<!--<ul>
							<li>
								<a href="index.html">Home</a>
							</li>
							<li>
								<a href="search.php">Search</a>
							</li>
							<li>
								<a href="calender.html">Calender</a>
							</li>
							<li>
								<a href="council.html">Council</a>
							</li>
							<li>
								<a href="coffee.html">Coffee</a>
							</li>
							<li>
								<a href="food.html">Food</a>
							</li>
							<li>
								<a href="food_search.html">Food Search</a>
							</li>
							<li>
								<a href="map.html">Map</a>
							</li>
						</ul>-->
                        <ul>
						<li>
							<a href="index.php">Home</a>
						</li>
						<li>
							<a href="search.php">Search</a>
						</li>
						<li>
							<a href="remind.php">Calender</a>
						</li>
						<li>
							<a href="council.php">Council</a>
						</li>
						<li>
							<a href="coffee.html">Coffee</a>
						</li>
						<li>
							<a href="food.html">Food</a>
						</li>
						<li>
							<a href="food_search.html">Food Search</a>
						</li>
						<li>
							<a href="map.php">Map</a>
						</li>
						<li>
							<a href="welcome.html">Welcome</a>
						</li>
						<li>
							<a href="select.html">Select</a>
						</li>
						<li>
							<a href="item.html">Item</a>
						</li>
						<li>
							<a href="item_tag.html">Item tag</a>
						</li>
						<li>
							<a href="create_council.html">Create council</a>
						</li>
						<li>
							<a href="category.html">Category</a>
						</li>
						<li>
							<a href="business.html">Business</a>
						</li>
					</ul>
					</div>	
				</div>
			</div>