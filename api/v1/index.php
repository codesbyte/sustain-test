<?php

/**
 * This file is the index file for the Sustain Me API.
 *
 * @author     Alex Portlock (alex.portlock@sustainme.com.au)
 * @copyright  2015 The Sustain Me Group
 */

require_once '../include/DatabaseHandler.php';
require_once '../include/PassHash.php';
require '.././libs/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

/* ------------------ Utility Functions ------------------ */

/**
 * Adds a layer to authenticate whether the caller has a valid API key.
 */
function authenticate(\Slim\Route $route) {
	// Gets the headers
	$headers = apache_request_headers();
	$response = array();
	$app = \Slim\Slim::getInstance();

    if (isset($headers['Authorization'])) {
		// If the Auth header is present
        $db = new DatabaseHandler();

        $api_key = $headers['Authorization'];

        if (!$db->isValidApiKey($api_key)) {
            $response["error"] = true;
            $response["message"] = "Invalid API key";
            echoResponse(401, $response);
            $app->stop();
        }
		
    } else {
        // If the Auth header is not present
        $response["error"] = true;
        $response["message"] = "Invalid API key.";
        echoResponse(400, $response);
        $app->stop();
    }
}

/**
 * Verifies that the parameters required are present.
 * @param An array of the required parameters
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
	
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    
	// If the field doesn't exist in the request params OR the field does exist and is just whitespace
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // If not all required fields are present, echo an error message and stop the app.
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' missing or empty';
        echoResponse(400, $response);
        $app->stop();
    }
}

/**
 * Echos the Json response
 * @param string status_code - the HTTP status code
 * @param integer response - the Json response
 */
function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    
	//Sets the status code
    $app->status($status_code);

	//Sets the headers required for CORS
	$res = $app->response();
	$res->header('Access-Control-Allow-Origin', '*');
	$res->header('Access-Control-Allow-Headers', 'Content-Type, X-Requested-With, X-authentication, X-client, Authorization');
	$res->header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');

    //Sets the content return type to Json
    $app->contentType('application/json');

    echo json_encode($response);
}

/**
 * The options response for the browser "preflight" request
 */
$app->options('/(:name+)', function() use ($app) {
$response = $app->response();
    $app->response()->status(200);
    $response->header('Access-Control-Allow-Origin', '*'); 
    $response->header('Access-Control-Allow-Headers', 'Content-Type, X-Requested-With, X-authentication, X-client, Authorization');
    $response->header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');

});

/* ------------------ Push Notifications ------------------ */
$app->get('/push/:id', 'authenticate', function($council_id) {
	$response = array();
	
	// Handle the request and get the councils
	$db = new DatabaseHandler();
	$result = $db->pushNotificationData($council_id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;
        $response["results"] = array();
	
        // For each result, add it to the response
        foreach($result as $row) {
            $tmp = array();            
            $tmp["deviceType"] = $row["deviceType"];
            $tmp["pushNotificationID"] = $row["pushNotificationID"];
            array_push($response["results"], $tmp);
        }
		
		echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Council doesn't exist";
		
		echoResponse(404, $response);
	}
});


/* ------------------ Council Functions ------------------ */

/**
 * Create a council
 * method POST
 * url /council
 */
$app->post('/council', 'authenticate', function() use ($app){
	// Check for the required params
    verifyRequiredParams(array('name'));
	
	// Set up the variables for the creation of the council
	$response = array();
	$name = $app->request->post('name');
	$address = $app->request->post('address');
	$openinghours = $app->request->post('openinghours');
	$url = $app->request->post('url');
	$phonenumber = $app->request->post('phonenumber');
	$rssfeed = $app->request->post('rssfeed');

	// Handle the request and create the council
	$db = new DatabaseHandler();
	$council_id = $db->createCouncil($name, $address, $openinghours, $url, $phonenumber, $rssfeed);

	// Echoes the response to the client
	if ($council_id != NULL) {
		$response["error"] = false;
		$response["message"] = "Council created successfully";
		$response["council_id"] = $council_id;
		echoResponse(201, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Failed to create council";
		echoResponse(200, $response);
	}
});
 
/**
 * Get all councils
 * method GET
 * URL /council
 */
$app->get('/council', 'authenticate', function() {

	// Handle the request and get the councils
	$db = new DatabaseHandler();
	$result = $db->getAllCouncils();
	
	// Set up the response
	$response = array();
	$response["error"] = false;
	$response["councils"] = array();
	
	// For each result, add it to the response
	foreach($result as $row) {
		$tmp = array();
		$tmp["id"] = $row["id"];
		$tmp["name"] = $row["name"];
		$tmp["address"] = $row["address"];
		$tmp["openinghours"] = $row["openinghours"];
		$tmp["url"] = $row["url"];
		$tmp["phonenumber"] = $row["phonenumber"];
		$tmp["rssfeed"] = $row["rssfeed"];
                $tmp["data_hash"] = $row["data_hash"];
                $tmp["data_hash"] = $row["data_hash"];
                $tmp["recycling_start"] = $row["recycling_start"];
		$tmp["isBackToEarth"] = $row["isBackToEarth"];
		$tmp["logo_filename"] = $row["logo_filename"];
		$tmp["emailaddress"] = $row["emailaddress"];
		$tmp["calendarSignedUp"] = $row["calendarSignedUp"];
                $tmp["recycle_bin_colour"] = $row["recycle_bin_colour"];
                $tmp["greenwaste_bin_colour"] = $row["greenwaste_bin_colour"];
                $tmp["garbage_link"] = $row["garbage_link"];
                $tmp["recycling_link"] = $row["recycling_link"];
                $tmp["dob_in_dump"] = $row["dob_in_dump"];

		array_push($response["councils"], $tmp);
	}
	
	// Echoes the response to the client
	echoResponse(200, $response);
});

/**
 * Get a single council
 * method GET
 * URL /council
 */
$app->get('/council/:id', 'authenticate', function($council_id) {
	$response = array();
	
	// Handle the request and get the councils
	$db = new DatabaseHandler();
	$result = $db->getCouncil($council_id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;	
		$response["id"] = $result["id"];
		$response["name"] = $result["name"];
		$response["address"] = $result["address"];
		$response["openinghours"] = $result["openinghours"];
		$response["url"] = $result["url"];
		$response["phonenumber"] = $result["phonenumber"];
		$response["rssfeed"] = $result["rssfeed"];
                $response["data_hash"] = $result["data_hash"];
                $response["recycling_start"] = $result["recycling_start"];
		$response["isBackToEarth"] = $result["isBackToEarth"];
		$response["logo_filename"] = $result["logo_filename"];
		$response["emailaddress"] = $result["emailaddress"];
		$response["calendarSignedUp"] = $result["calendarSignedUp"];
                $response["recycle_bin_colour"] = $result["recycle_bin_colour"];
                $response["greenwaste_bin_colour"] = $result["greenwaste_bin_colour"];
                $response["garbage_link"] = $result["garbage_link"];
                $response["recycling_link"] = $result["recycling_link"];
                $response["dob_in_dump"] = $result["dob_in_dump"];
		
		echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Council doesn't exist";
		
		echoResponse(404, $response);
	}
});

/**
 * Get a single council and its area information
 * method GET
 * URL /council
 */
$app->get('/council/:id/:a_id', 'authenticate', function($council_id, $area_id) {
	$response = array();
	
	// Handle the request and get the councils
	$db = new DatabaseHandler();
	$result = $db->getCouncilArea($council_id, $area_id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;	
		$response["id"] = $result["id"];
		$response["name"] = $result["name"];
		$response["address"] = $result["address"];
		$response["openinghours"] = $result["openinghours"];
		$response["url"] = $result["url"];
		$response["phonenumber"] = $result["phonenumber"];
		$response["rssfeed"] = $result["rssfeed"];
                $response["data_hash"] = $result["data_hash"];
                $response["recycling_start"] = $result["recycling_start"];
		$response["isBackToEarth"] = $result["isBackToEarth"];
		$response["logo_filename"] = $result["logo_filename"];
		$response["emailaddress"] = $result["emailaddress"];
		$response["calendarSignedUp"] = $result["calendarSignedUp"];
                $response["recycle_bin_colour"] = $result["recycle_bin_colour"];
                $response["greenwaste_bin_colour"] = $result["greenwaste_bin_colour"];
                $response["garbage_link"] = $result["garbage_link"];
                $response["recycling_link"] = $result["recycling_link"];
	        $response["dob_in_dump"] = $result["dob_in_dump"];
	
		echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Council or council area doesn't exist";
		
		echoResponse(404, $response);
	}
});

/* ------------------ Event Functions ------------------ */
/**
 * Get all events for a council/area pair
 * method GET
 * URL /event
 */
$app->get('/event/:id/:a_id', 'authenticate', function($council_id, $area_id) {
	$response = array();
	
	// Handle the request and get the councils
	$db = new DatabaseHandler();
	$result = $db->getEvents($council_id, $area_id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;
        $response["events"] = array();
        
        foreach($result as $row) {
            $tmp = array();
            $tmp["type_id"] = $row["type_id"];
            $tmp["start_date"] = $row["start_date"];
            $tmp["duration"] = $row["duration"];
            array_push($response["events"], $tmp);
        }
		
        echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "No events for this council/area";
		
		echoResponse(404, $response);
	}
});

/* ------------------ Event Type Functions ------------------ */
/**
 * Get all event types available for a council/area pair
 * method GET
 * URL /event
 */
$app->get('/event_types/:id/:a_id', 'authenticate', function($council_id, $area_id) {
	$response = array();
	
	// Handle the request and get the councils
	$db = new DatabaseHandler();
	$result = $db->getEventTypes($council_id, $area_id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;
        $response["event_types"] = array();
        
        foreach($result as $row) {
            $tmp = array();
            $tmp["id"] = $row["id"];
            $tmp["name"] = $row["name"];
            $tmp["colour"] = $row["colour"];
            $tmp["link"] = $row["link"];
            array_push($response["event_types"], $tmp);
        }
		
        echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "No events for this council/area";
		
		echoResponse(404, $response);
	}
});



/* ------------------ Business Functions ------------------ */

/**
 * Create a business
 * method POST
 * url /business
 */
$app->post('/business', 'authenticate', function() use ($app){
	// Check for the required params
    verifyRequiredParams(array('name'));

	// Set up the variables for the creation of the business
	$response = array();
	$name = $app->request->post('name');
	$url = $app->request->post('url');
	$address = $app->request->post('address');
	$phone_number = $app->request->post('phone_number');

	// Handle the request and create the business
	$db = new DatabaseHandler();
	$business_id = $db->createBusiness($name, $url, $address, $phone_number);

	// Echoes the response to the client
	if ($business_id != NULL) {
		$response["error"] = false;
		$response["message"] = "Business created successfully";
		$response["business_id"] = $business_id;
		echoResponse(201, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Failed to create business";
		echoResponse(200, $response);
	}
});
 
/**
 * Get all businesses
 * method GET
 * URL /business
 */
$app->get('/business', 'authenticate', function() {
	// Handle the request and get the businesses
	$db = new DatabaseHandler();
	$result = $db->getAllBusinesses();
	
	// Set up the response
	$response = array();
	$response["error"] = false;
	$response["businesses"] = array();
	
	// For each result, add it to the response
	foreach($result as $row) {
		$tmp = array();
		$tmp["id"] = $row["id"];
		$tmp["name"] = $row["name"];
		$tmp["url"] = $row["url"];
		$tmp["address"] = $row["address"];
		$tmp["phone_number"] = $row["phone_number"];
		array_push($response["businesses"], $tmp);
	}

	// Echoes the response to the client
	echoResponse(200, $response);
});

/**
 * Get a single business
 * method GET
 * URL /business
 */
$app->get('/business/:id', 'authenticate', function($business_id) {
	$response = array();
	
	// Handle the request and get the businesses
	$db = new DatabaseHandler();
	$result = $db->getBusiness($business_id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;	
		$response["id"] = $result["id"];
		$response["name"] = $result["name"];
		$response["url"] = $result["url"];
		$response["address"] = $result["address"];
		
		echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Business doesn't exist";
		echoResponse(404, $response);
	}
});


/* ------------------ User Functions ------------------ */
/**
 * Create a user, checks if a user already exists
 * method POST
 * url /user
 * DEPLOYMENT 
 */
$app->post('/userDebug', 'authenticate', function() use ($app) {
		// Check for the required params
		verifyRequiredParams(array('deviceID', 'deviceType', 'location', 'council_id'));

		// Set up the variables for the creation of the user
		$response = array();

		$id = $app->request->post('deviceID');
                $deviceType = $app->request->post('deviceType');
                $pushNotificationID = $app->request->post('pushNotificationID');
		$location = $app->request->post('location');
		$council_id = $app->request->post('council_id');
                $area_id = $app->request->post('area_id');

    
		// Handle the request and create the user
		$db = new DatabaseHandler();
		$res = $db->createUser($id, $deviceType, $pushNotificationID, $location, $council_id, $area_id);

		// Echoes the response to the client
		if ($res == USER_CREATE_FAILED) {
			$response["error"] = true;
			$response["message"] = "User creation failed";
		} else {
                        $response["error"] = false;
			$response["message"] = "User creation successful";
                        $response["id"] = $res;
                }
		
		echoResponse(201, $response);
	});
/**
 * Create a user, checks if a user already exists
 * method POST
 * url /user
 * DEBUG
 */
$app->post('/user', 'authenticate', function() use ($app) {
		// Check for the required params
		verifyRequiredParams(array('deviceID', 'deviceType', 'location', 'council_id'));

		// Set up the variables for the creation of the user
		$response = array();

		$id = $app->request->post('deviceID');
                $deviceType = $app->request->post('deviceType');
                $pushNotificationID = $app->request->post('pushNotificationID');
		$location = $app->request->post('location');
		$council_id = $app->request->post('council_id');
                $area_id = $app->request->post('area_id');

    
		// Handle the request and create the user
		$db = new DatabaseHandler();
		$res = $db->createUser($id, $deviceType, $pushNotificationID, $location, $council_id, $area_id);

		// Echoes the response to the client
		if ($res == USER_CREATE_FAILED) {
			$response["error"] = true;
			$response["message"] = "User creation failed";
		} else if ($res == USER_ALREADY_EXISTED) {
			$response["error"] = true;
			$response["message"] = "User already exists";
			$response["deviceID"] = $id;
        } else {
            $response["error"] = false;
	    $response["message"] = "User creation successful";
            $response["id"] = $res;
        }
		
		echoResponse(201, $response);
	});


/**
 * Get a single user
 * method GET
 * url /user
 */
$app->get('/user/:id', 'authenticate', function($id) {
	$response = array();
	
	// Handle the request and get the user
	$db = new DatabaseHandler();
	$result = $db->getUser($id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;	
		$response["id"] = $result["id"];
        $response["deviceID"] = $result["deviceID"];
        $response["deviceType"] = $result["deviceType"];
        $response["pushNotificationID"] = $result["pushNotificationID"];
        $response["location"] = $result["location"];		
		$response["council_id"] = $result["council_id"];
        $response["area_id"] = $result["area_id"];
		
		echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "The requested user doesn't exist";
		echoResponse(404, $response);
	}
});

/**
 * Get a single user
 * method GET
 * url /user
 
$app->get('/userpush/:id', 'authenticate', function($id) {
	$response = array();
	
	// Handle the request and get the user
	$db = new DatabaseHandler();
	$result = $db->getPushIds($id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;	
                $response["deviceType"] = $result["deviceType"];
                $response["pushNotificationID"] = $result["pushNotificationID"];
		
		echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "The requested user doesn't exist";
		echoResponse(404, $response);
	}
});
*/
/**
 * Create a user, checks if a user already exists
 * method POST
 * url /user
 */
$app->put('/user/:id', 'authenticate', function($id) use ($app) {
		
        if ($app->request->put('deviceID') === NULL)
        {
			if ($app->request->put('pushNotificationID') === NULL)
			{
				verifyRequiredParams(array('location', 'council_id', 'area_id'));
				
				$response = array();
				$location = $app->request->put('location');
				$council_id = $app->request->put('council_id');
				$area_id = $app->request->put('area_id');
				
				$db = new DatabaseHandler();
				$res = $db->updateUserLocation($id, $location, $council_id, $area_id);
				
				// Echoes the response to the client
				if ($res == USER_UPDATE_FAILED) {
					$response["error"] = true;
					$response["message"] = "User update failed, user doesn't exist";
				} else {
					$response["error"] = true;
					$response["message"] = "User update successful";
				}

				echoResponse(201, $response);
			}
			else
			{
				verifyRequiredParams(array('pushNotificationID'));

				// Set up the variables for the creation of the user
				$response = array();
				$pushNotificationID = $app->request->put('pushNotificationID');

				// Handle the request and create the user
				$db = new DatabaseHandler();
				$res = $db->updateUserPush($id, $pushNotificationID);

				// Echoes the response to the client
				if ($res == USER_UPDATE_FAILED) {
					$response["error"] = true;
					$response["message"] = "User update failed, user doesn't exist";
				} else {
					$response["error"] = true;
					$response["message"] = "User update successful";
				}

				echoResponse(201, $response);
			}
        }
		else
        {
            // Check for the required params
            verifyRequiredParams(array('deviceID', 'deviceType', 'pushNotificationID', 'location', 'council_id', 'area_id'));

            // Set up the variables for the creation of the user
            $response = array();
            $deviceID = $app->request->put('deviceID');
            $deviceType = $app->request->put('deviceType');
            $pushNotificationID = $app->request->put('pushNotificationID');
            $location = $app->request->put('location');
            $council_id = $app->request->put('council_id');
            $area_id = $app->request->put('area_id');

            // Handle the request and create the user
            $db = new DatabaseHandler();
            $res = $db->updateUser($id, $deviceID, $deviceType, $pushNotificationID, $location, $council_id, $area_id);

            // Echoes the response to the client
            if ($res == USER_UPDATE_FAILED) {
                $response["error"] = true;
                $response["message"] = "User update failed, user doesn't exist";
            } else {
                $response["error"] = true;
                $response["message"] = "User update successful";
            }

            echoResponse(201, $response);
        }
        
	});

/* ------------------ User Dates Functions ------------------ */
/**
 * Create a user date entry
 * method POST
 * url /user_date
 */
$app->post('/user_date', 'authenticate', function() use ($app) {
		// Check for the required params
		verifyRequiredParams(array('user_id', 'percentage', 'bin_size'));

		// Set up the variables for the creation of a user date
		$response = array();
		$user_id = $app->request->post('user_id');
                $date = date("Y-m-d");
		$percentage = $app->request->post('percentage');
		$bin_size = $app->request->post('bin_size');
		
		// Handle the request and create the user date
		$db = new DatabaseHandler();
		$entry_successful = $db->createUserDate($user_id, $date, $percentage, $bin_size);

        // Echoes the response to the client
        if ($entry_successful != false) {
            $response["error"] = false;
            $response["message"] = "User date created successfully";
            echoResponse(201, $response);
        } else {
            $response["error"] = true;
            $response["message"] = "Failed to create user date";
            echoResponse(200, $response);
        }
	});

/**
 * Get all user dates for a single user
 * method GET
 * url /user
 */
$app->get('/user_date/:id', 'authenticate', function($user_id) {
    // Handle the request and get the businesses
	$db = new DatabaseHandler();
	$result = $db->getUserDates($user_id);
	
	// Set up the response
	$response = array();
	$response["error"] = false;
	$response["user_dates"] = array();
    
	// For each result, add it to the response
	foreach($result as $row) {
		$tmp = array();
		$tmp["user_id"] = $row["user_id"];
		$tmp["date"] = $row["date"];
		$tmp["percentage"] = $row["percentage"];
		$tmp["bin_size"] = $row["bin_size"];
		array_push($response["user_dates"], $tmp);
	}

	// Echoes the response to the client
	echoResponse(200, $response);
});

/* ------------------ Category Functions ------------------ */

/**
 * Create a category
 * method POST
 * url /category
 */
$app->post('/category', 'authenticate', function() use ($app){
	// Check for the required params
    verifyRequiredParams(array('name'));

	// Set up the variables for the creation of the category
	$response = array();
	$name = $app->request->post('name');

	// Handle the request and create the category
	$db = new DatabaseHandler();
	$category_id = $db->createCategory($name);

	// Echoes the response to the client
	if ($category_id != NULL) {
		$response["error"] = false;
		$response["message"] = "Category created successfully";
		$response["category_id"] = $category_id;
		echoResponse(201, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Failed to create category";
		echoResponse(200, $response);
	}
});
 
/**
 * Get all categories
 * method GET
 * URL /category
 */
$app->get('/category', 'authenticate', function() {
	// Handle the request and get the categories
	$db = new DatabaseHandler();
	$result = $db->getAllCategories();
	
	// Set up the response
	$response = array();
	$response["error"] = false;
	$response["categories"] = array();
	
	// For each result, add it to the response
	foreach($result as $row) {
		$tmp = array(); 
		$tmp["id"] = $row["id"];
		$tmp["name"] = $row["name"];
        $tmp["icon_filename"] = $row["icon_filename"];
		array_push($response["categories"], $tmp);
	}

	// Echoes the response to the client
	echoResponse(200, $response);
});

/**
 * Get a single category
 * method GET
 * URL /category
 */
$app->get('/category/:id', 'authenticate', function($category_id) {
	$response = array();
	
	// Handle the request and get the category
	$db = new DatabaseHandler();
	$result = $db->getCategory($category_id);

	$getItemByCategory = $db->getItemsByCategory($category_id,72);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;	
		$response["id"] = $result["id"];
		$response["name"] = $result["name"];
        $response["icon_filename"] = $result["icon_filename"];
        $response["category_items"] = $getItemByCategory;
		
		echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Category doesn't exist";
		echoResponse(404, $response);
	}
});


/* ------------------ Item Functions ------------------ */

/**
 * Create an item
 * method POST
 * URL /item
 */
$app->post('/item', 'authenticate', function() use ($app){
	// Check for the required params
    verifyRequiredParams(array('council_id', 'category_id', 'name', 'is_donate', 'information', 'tag_id'));

	// Set up the variables for the creation of the item
	$response = array();
	$council_id = $app->request->post('council_id');
	$category_id = $app->request->post('category_id');
	$name = $app->request->post('name');
	$is_donate = $app->request->post('is_donate');
	$information = $app->request->post('information');
	$tag_id = $app->request->post('tag_id');
	$business_id = $app->request->post('business_id');

	// Handle the request and create the item
	$db = new DatabaseHandler();
	$item_id = $db->createItem($council_id, $category_id, $name, $is_donate, $information, $tag_id, $business_id);

	// Echoes the response to the client
	if ($item_id != NULL) {
		$response["error"] = false;
		$response["message"] = "Item created successfully";
		$response["business_id"] = $item_id;
		echoResponse(201, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Failed to create item";
		echoResponse(200, $response);
	}
});
 
/**
 * Get an item based on a given council and/or search term
 * method GET
 * URL /item
 */
$app->get('/item', 'authenticate', function() use($app) {
	// Check for the required params
	verifyRequiredParams(array('council_id'));
	
	// Set up the database handler
	$db = new DatabaseHandler();
	
	//Get the council id from the header and check if there is a search term too
	$council_id = $_GET["council_id"];
	$search_term = (isset($_GET['search_term']) ? $_GET['search_term'] : null);
	
	//If the user is just looking for all items for a given council
	if ($search_term == NULL)
	{
		$result = $db->searchForItemsWithCouncil($council_id);
	}
	else //if a search term exists, find only those items whose item tags fit the search term
	{
		$result = $db->searchForItemsWithItemTag($council_id, $search_term);
	}

	//echoResponse(200, $result);

	// Set up the response
	$response = array();
	$response["error"] = false;
	$response["items"] = array();

	// For each result, add it to the response
	foreach($result as $row) {
		$tmp = array();
		$tmp = array();
		$tmp["id"] = $row["id"];
		$tmp["council_id"] = $row["council_id"];
		$tmp["category_id"] = $row["category_id"];
		$tmp["name"] = $row["name"];
		$tmp["is_donate"] = $row["is_donate"];
		$tmp["information"] = $row["information"];
		$tmp["tag_id"] = $row["tag_id"];
		$tmp["business_id"] = $row["business_id"];
		$tmp["item_tag"] = $row["item_tag"];
		$tmp["business_name"] = $row["business_name"];
		$tmp["business_url"] = $row["business_url"];
		$tmp["business_address"] = $row["business_address"];
		$tmp["business_phone_number"] = $row["business_phone_number"];
    		$tmp["icon_filename"] = $row["icon_filename"];
		$tmp["isBackToEarth"] = $row["isBackToEarth"];
		$tmp["logo_filename"] = $row["logo_filename"];
		$tmp["d_information"] = $row["d_information"];
		$tmp["d_business_id"] = $row["d_business_id"];
		$tmp["d_business_name"] = $row["d_business_name"];
		$tmp["d_business_url"] = $row["d_business_url"];
		$tmp["d_business_address"] = $row["d_business_address"];
		$tmp["d_business_phone_number"] = $row["d_business_phone_number"];
		$tmp["is_reuse"] = $row["is_reuse"];
		$tmp["ru_information"] = $row["ru_information"];
		$tmp["ru_business_id"] = $row["ru_business_id"];
		$tmp["ru_business_name"] = $row["ru_business_name"];
		$tmp["ru_business_url"] = $row["ru_business_url"];
		$tmp["ru_business_address"] = $row["ru_business_address"];
		$tmp["ru_business_phone_number"] = $row["ru_business_phone_number"];
		$tmp["is_recycle"] = $row["is_recycle"];
		array_push($response["items"], $tmp);
	}


	echoResponse(200, $response);
	
	
});

/*
	UPDATE ITEM
*/
$app->put('/item/:id', 'authenticate', function($id) use ($app) {
		
		$response = array();
		$tag_id = $app->request->put('tag_id');
		$is_recycle = $app->request->put('is_recycle');
		$information = $app->request->put('information');
		$business_id = $app->request->put('business_id');
		$is_donate = $app->request->put('is_donate');
		$d_business_id = $app->request->put('d_business_id');
		$d_information = $app->request->put('d_information');
		$is_reuse = $app->request->put('is_reuse');
		$ru_business_id = $app->request->put('ru_business_id');
		$ru_information = $app->request->put('ru_information');

		$db = new DatabaseHandler();
		$res = $db->updateItem($id, $tag_id, $information, $business_id, $is_recycle, $is_donate, $d_business_id, $d_information, $is_reuse, $ru_business_id, $ru_information);
		
		if($res != NULL){
			$response["error"] = false;
			$response["message"] = "Item update successful";
		}else{
			$response["error"] = true;
			$response["message"] = "Item update failed";
		}
		echoResponse(201, $response);
});

/**
 * Get a single item
 * method GET
 * URL /category
 */
$app->get('/item/:id', 'authenticate', function($item_id) {
	$response = array();
	
	// Handle the request and get the category
	$db = new DatabaseHandler();
	$result = $db->getItem($item_id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;	
		$response["item"] = array();
		$tmp = array();
		$tmp["id"] = $result["id"];
		$tmp["council_id"] = $result["council_id"];
		$tmp["category_id"] = $result["category_id"];
		$tmp["name"] = $result["name"];
		$tmp["is_donate"] = $result["is_donate"];
		$tmp["information"] = $result["information"];
		$tmp["tag_id"] = $result["tag_id"];
		$tmp["business_id"] = $result["business_id"];
		$tmp["item_tag"] = $result["item_tag"];
		$tmp["business_name"] = $result["business_name"];
		$tmp["business_url"] = $result["business_url"];
		$tmp["business_address"] = $result["business_address"];
		$tmp["business_phone_number"] = $result["business_phone_number"];
		$tmp["icon_filename"] = $result["icon_filename"];
		$tmp["isBackToEarth"] = $result["isBackToEarth"];
		$tmp["logo_filename"] = $result["logo_filename"];
		$tmp["d_information"] = $result["d_information"];
		$tmp["d_business_id"] = $result["d_business_id"];
		$tmp["d_business_name"] = $result["d_business_name"];
		$tmp["d_business_url"] = $result["d_business_url"];
		$tmp["d_business_address"] = $result["d_business_address"];
		$tmp["d_business_phone_number"] = $result["d_business_phone_number"];
		$tmp["is_reuse"] = $result["is_reuse"];
		$tmp["ru_information"] = $result["ru_information"];
		$tmp["ru_business_id"] = $result["ru_business_id"];
		$tmp["ru_business_name"] = $result["ru_business_name"];
		$tmp["ru_business_url"] = $result["ru_business_url"];
		$tmp["ru_business_address"] = $result["ru_business_address"];
		$tmp["ru_business_phone_number"] = $result["ru_business_phone_number"];
		$tmp["is_recycle"] = $result["is_recycle"];
		array_push($response["item"], $tmp);
		echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Item doesn't exist";
		echoResponse(404, $response);
	}
});

/* ------------------ Item Tag Functions ------------------ */

/**
 * Create a item_tag
 * method POST
 * url /item_tag
 */
$app->post('/item_tag', 'authenticate', function() use ($app){
	// Check for the required params
    verifyRequiredParams(array('information'));

	// Set up the variables for the creation of the item tag
	$response = array();
	$information = $app->request->post('information');

	// Handle the request and create the item tag
	$db = new DatabaseHandler();
	$item_tag_id = $db->createItemTag($information);

	// Echoes the response to the client
	if ($item_tag_id != NULL) {
		$response["error"] = false;
		$response["message"] = "Item tag created successfully";
		$response["item_tag_id"] = $item_tag_id;
		echoResponse(201, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Failed to create item tag";
		echoResponse(200, $response);
	}
});
 
/**
 * Get all item_tags
 * method GET
 * URL /item_tag
 */
$app->get('/item_tag', 'authenticate', function() {
		// Handle the request and get the item tags
		$db = new DatabaseHandler();
		$result = $db->getAllItemTags();

		// Set up the response
		$response = array();
		$response["error"] = false;
		$response["item_tags"] = array();
		
		// For each result, add it to the response
		foreach($result as $row) {
			$tmp = array();
			$tmp["id"] = $row["id"];
			$tmp["information"] = $row["information"];
			array_push($response["item_tags"], $tmp);
		}

		// Echoes the response to the client
		echoResponse(200, $response);
});

/**
 * Get a single item_tag
 * method GET
 * URL /item_tag
 */
$app->get('/item_tag/:id', 'authenticate', function($item_tag_id) {
	$response = array();
	
	// Handle the request and get the item tag
	$db = new DatabaseHandler();
	$result = $db->getItemTag($item_tag_id);
	
	// Echoes the response to the client
	if ($result != NULL) {
		$response["error"] = false;	
		$response["id"] = $result["id"];
		$response["information"] = $result["information"];
		
		echoResponse(200, $response);
	} else {
		$response["error"] = true;
		$response["message"] = "Item tag doesn't exist";
		echoResponse(404, $response);
	}
});

$app->run();
?>