<?php

$headers = apache_request_headers();

if (isset($headers['Authorization'])) 
{
    $connection = NULL;
    
    try
    {
        $connection = new PDO('mysql:host=localhost;dbname=sustainm_SustainMeDB', 'sustainm_admin', 'OxfamMonash2013');
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $e)
    {
        echo $e->getMessage();
        die();
    }

    $api_key = $headers['Authorization'];
    $stmt = $connection->prepare("SELECT id from admin WHERE api_key = ?");
    $stmt->bindParam(1, $api_key, PDO::PARAM_STR);
    $stmt->execute();
    $stmt->fetch();
    $num_rows = $stmt->rowCount();

    if ($num_rows > 0) 
    {
        $stmt = $connection->prepare("SELECT deviceType, pushNotificationID FROM user WHERE council_id = ?");
        $stmt->bindParam(1, intval($_GET["id"]), PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        if ($stmt->rowCount() <= 0)
        {
            echo "No results found.";
        }

        $androidPhones = array();
        $iOSPhones = array();

        foreach($result as $row)
        {
            if (!is_null($row['pushNotificationID']))
            {
                if ($row['deviceType'] == "Android")
                    array_push($androidPhones, $row['pushNotificationID']);
                else if ($row['deviceType'] == "iOS")
                    array_push($iOSPhones, $row['pushNotificationID']);
            }
        }

 	if(!empty($androidPhones)){
	        //Android
	        // API access key from Google API's Console
		define( 'API_ACCESS_KEY', 'AIzaSyA2TkgJIRXg1k2jCYtcLZPMoPmJz2YGOos' );
		
		
		$registrationIds = $androidPhones;
		
		// prep the bundle
		$msg = array
		(
		    'message'     => 'Keep Calm and Use Phonegap',
		    'title'     => 'Phonegap Push Notification'
		);
		
		$fields = array
		(
		    'registration_ids'  => $registrationIds,
		    'data'              => $msg
		);
		
		$headers = array
		(
		    'Authorization: key=' . API_ACCESS_KEY,
		    'Content-Type: application/json'
		);
		
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		
		echo $result;
	}      
        
        // IOS PART
        // Put your device token here (without spaces):
        /*if(!empty($iOSPhones)){
		$deviceToken = '51f7debd24dd4450804bd69ca9b4ea52201b17d59c2421e0ab09fc2b8345191a';

		// Put your private key's passphrase here:
		$passphrase = '';

		// Put your alert message here:
		$message = 'My first push notification!';

		////////////////////////////////////////////////////////////////////////////////

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'apns-dev-cert.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		echo 'Connected to APNS' . PHP_EOL;

		// Create the payload body
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
			);

		// Encode the payload as JSON
		$payload = json_encode($body);

		foreach($iOSPhones as $row)
		{
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $row) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
		}

		//if (!$result)
		//	echo 'Message not delivered' . PHP_EOL;
		//else
		//	echo 'Message successfully delivered' . PHP_EOL;

		// Close the connection to the server
		fclose($fp);
	}*/

    }
    else
    {
        echo "API key incorrect.";
    }

} 
else 
{
    echo "Authorization header not set.";
}

?>