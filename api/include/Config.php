<?php
/**
 * Database configuration
 */
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'sustainm_SustainMeDB');

define('USER_CREATED_SUCCESSFULLY', 0);
define('USER_CREATE_FAILED', 1);
define('USER_ALREADY_EXISTED', 2);
define('USER_DOESNT_EXIST', 3);
define('USER_UPDATE_SUCCESSFUL', 4);
define('USER_UPDATE_FAILED', 5);
?>
