<?php

/**
 * This class handles all connections to the Sustain Me database. Relies on a PDO connection made in DatabaseConnection.
 *
 
 * @author     Alex Portlock (alex.portlock@sustainme.com.au)
 * @copyright  2015 The Sustain Me Group
 */
class DatabaseHandler {

    private $connection;

	/**
	* The class constuctor. Makes a connection to the database.
	*/
    function __construct() {
        require_once dirname(__FILE__) . '/DatabaseConnection.php';
        $db = new DatabaseConnection();
        $this->connection = $db->connect();
    }
	
	/* ------------------ VALIDATION METHODS ------------------ */
	/**
	 * Validates that the client API key is a valid key
	 * @return boolean
	 */
	public function isValidApiKey($api_key) {
		$stmt = $this->connection->prepare("SELECT id from admin WHERE api_key = ?");
		$stmt->bindParam(1, $api_key, PDO::PARAM_STR);
		$stmt->execute();
		$stmt->fetch();
		$num_rows = $stmt->rowCount();
		return $num_rows > 0;
	}
	
    /* ------------------PUSH NOTIFICATION METHODS ------------------ */        
    public function pushNotificationData($council_id) {
        $stmt = $this->connection->prepare("SELECT u.deviceType, u.pushNotificationID 
                                            FROM  `user` u
                                            JOIN  `council` c ON u.council_id = c.id
                                            WHERE c.id = ?");
		$stmt->bindParam(1, $council_id, PDO::PARAM_INT);
        
		if ($stmt->execute(array($council_id))) {
			$result = $stmt->fetchAll();        
			return $result;
		}
		else {
            return NULL;
        }
    }
    
    
	/* ------------------ USER METHODS ------------------ */
	/**
	* Creates a user in the database. Is not autoassigned an ID by the database and should be assigned PhoneGap's Universally Unique Identifier instead (device.uuid).
	* @return An integer (as defined in config.php)
	*/
	public function createUserDebug($deviceID, $deviceType, $pushNotificationID, $location, $council_id, $area_id) {
		
		if (!$this->userExists($deviceID)) {
                   if ($area_id == null){
                      $area_id = 1;
                   }
            
			$stmt = $this->connection->prepare("INSERT INTO user(deviceID, deviceType, pushNotificationID, location, council_id, area_id) VALUES(?, ?, ?, ?, ?, ?)");
			$stmt->bindParam(1, $deviceID, PDO::PARAM_STR);
			$stmt->bindParam(2, $deviceType, PDO::PARAM_STR);
                        $stmt->bindParam(3, $pushNotificationID, PDO::PARAM_STR);
                        $stmt->bindParam(4, $location, PDO::PARAM_STR);
			$stmt->bindParam(5, $council_id, PDO::PARAM_INT);
                        $stmt->bindParam(6, $area_id, PDO::PARAM_INT);
			
			if($stmt->execute(array($deviceID, $deviceType, $pushNotificationID, $location, $council_id, $area_id))) 
				return $this->connection->lastInsertId();
			else
				return USER_CREATE_FAILED;
		}
		else
			return USER_ALREADY_EXISTED;	
    }

	/**
	* Creates a user in the database. Is not autoassigned an ID by the database and should be assigned PhoneGap's Universally Unique Identifier instead (device.uuid).
	* @return An integer (as defined in config.php)
	*/
	public function createUser($deviceID, $deviceType, $pushNotificationID, $location, $council_id, $area_id) {
		
		if (!$this->userExists($deviceID)) {
                   if ($area_id == null){
                      $area_id = 1;
                   }
            
			$stmt = $this->connection->prepare("INSERT INTO user(deviceID, deviceType, pushNotificationID, location, council_id, area_id) VALUES(?, ?, ?, ?, ?, ?)");
			$stmt->bindParam(1, $deviceID, PDO::PARAM_STR);
			$stmt->bindParam(2, $deviceType, PDO::PARAM_STR);
                        $stmt->bindParam(3, $pushNotificationID, PDO::PARAM_STR);
                        $stmt->bindParam(4, $location, PDO::PARAM_STR);
			$stmt->bindParam(5, $council_id, PDO::PARAM_INT);
                        $stmt->bindParam(6, $area_id, PDO::PARAM_INT);
			
			if($stmt->execute(array($deviceID, $deviceType, $pushNotificationID, $location, $council_id, $area_id))) 
				return $this->connection->lastInsertId();
			else
				return USER_CREATE_FAILED;
		}
		else {
                        $stmt = $this->connection->prepare("SELECT * FROM user WHERE deviceID= ?");
		        $stmt->bindParam(1, $deviceID, PDO::PARAM_STR);
		
		        if ($stmt->execute(array($deviceID))) {
		       	    $result = $stmt->fetch();
       			    $uresult = $this->updateUser($result['id'], $deviceID, $deviceType, $pushNotificationID, $location, $council_id, $area_id);
	                   if($uresult == USER_UPDATE_SUCCESSFUL){
                               return $result['id'];
                           }else return $uresult;
		        } else {
                            return NULL;
                        }             
                }
    }
	
	/**
	* Checks if a given ID already exists.
	* @return boolean
	*/
	private function userExistsDebug($id) {
		$stmt = $this->connection->prepare("SELECT id from user WHERE id = ? LIMIT 1");
		$stmt->bindParam(1, $id, PDO::PARAM_INT);
		$stmt->execute(array($id));
		
		return $stmt->rowCount() > 0 ? TRUE : FALSE;
        }
	
	/**
	* Checks if a given device ID already exists.
	* @return boolean
	*/
	private function userExists($id) {
		$stmt = $this->connection->prepare("SELECT id from user WHERE deviceID = ? LIMIT 1");
		$stmt->bindParam(1, $id, PDO::PARAM_INT);
		$stmt->execute(array($id));
		
		return $stmt->rowCount() > 0 ? TRUE : FALSE;
        }
	
	/**
	* Checks if a given ID already exists.
	* @return boolean
	*/
	private function userExistsWithId($id) {
		$stmt = $this->connection->prepare("SELECT id from user WHERE id = ? LIMIT 1");
		$stmt->bindParam(1, $id, PDO::PARAM_INT);
		$stmt->execute(array($id));
		
		return $stmt->rowCount() > 0 ? TRUE : FALSE;
        }
	
        /**
	* GETs a single user with a given id.
	* @return A single row from the database.
	*/
	public function getUser($id) {
        $stmt = $this->connection->prepare("SELECT * FROM user u WHERE u.id = ?");
		$stmt->bindParam(1, $user_id, PDO::PARAM_STR);
		
		if ($stmt->execute(array($id))) {
			$result = $stmt->fetch();
			return $result;
		}
		else {
            return NULL;
        }
    }
    
    public function updateUser($id, $deviceID, $deviceType, $pushNotificationID, $location, $council_id, $area_id) {
        if ($this->userExistsWithId($id)) {
            $stmt = $this->connection->prepare("UPDATE user SET deviceID = ?, deviceType = ?, pushNotificationID = ?, location = ?, council_id = ?, area_id = ? WHERE id = ?");
            $stmt->bindParam(1, $deviceID, PDO::PARAM_STR);
			$stmt->bindParam(2, $deviceType, PDO::PARAM_STR);
            $stmt->bindParam(3, $pushNotificationID, PDO::PARAM_STR);
            $stmt->bindParam(4, $location, PDO::PARAM_STR);
			$stmt->bindParam(5, $council_id, PDO::PARAM_INT);
            $stmt->bindParam(6, $area_id, PDO::PARAM_INT);
            $stmt->bindParam(7, $id, PDO::PARAM_INT);
            
            if($stmt->execute(array($deviceID, $deviceType, $pushNotificationID, $location, $council_id, $area_id, $id)))
                return USER_UPDATE_SUCCESSFUL;
			else
				return USER_UPDATE_FAILED;
        }
        else
			return USER_DOESNT_EXIST;
    }
    
    public function updateUserPush($id, $pushNotificationID) {
        if ($this->userExistsWithId($id)) {
            $stmt = $this->connection->prepare("UPDATE user SET pushNotificationID = ? WHERE id = ?");
            $stmt->bindParam(1, $pushNotificationID, PDO::PARAM_STR);
            $stmt->bindParam(2, $id, PDO::PARAM_INT);
            
            if($stmt->execute(array($pushNotificationID, $id)))
                return $this->connection->lastInsertId();
			else
				return USER_UPDATE_FAILED;
        }
        else
			return USER_DOESNT_EXIST;
    }
	
	public function updateUserLocation($id, $location, $council_id, $area_id) {
        if ($this->userExists($id)) {
            $stmt = $this->connection->prepare("UPDATE user SET location = ?, council_id = ?, area_id = ? WHERE id = ?");
            $stmt->bindParam(1, $location, PDO::PARAM_STR);
			$stmt->bindParam(2, $council_id, PDO::PARAM_STR);
			$stmt->bindParam(3, $area_id, PDO::PARAM_STR);
            $stmt->bindParam(4, $id, PDO::PARAM_INT);
            
            if($stmt->execute(array($location, $council_id, $area_id, $id)))
                return $this->connection->lastInsertId();
			else
				return USER_UPDATE_FAILED;
        }
        else
			return USER_DOESNT_EXIST;
    }
    /*
    public function getPushIds($council_id){
        $stmt = $this->connection->prepare("SELECT * FROM user u WHERE u.council_id = ?");
		$stmt->bindParam(1, $council_id, PDO::PARAM_INT);
		
		if ($stmt->execute(array($council_id))) {
			$result = $stmt->fetch();
			return $result;
		}
		else{
                     return NULL
                }
    }

*/
    /* ------------------ USER DATE METHODS ------------------ */
    /**
	* Creates a user date in the database, which is a recycling entry by a user on a specific date
	* @return Returns true if successful and false if unsuccessful
	*/
    public function createUserDate($user_id, $date, $percentage, $bin_size) {
        $stmt = $this->connection->prepare("INSERT INTO user_date(user_id, date, percentage, bin_size) VALUES(?, ?, ?, ?)");
        $stmt->bindParam(1, $user_id, PDO::PARAM_STR);
        $stmt->bindParam(2, $date, PDO::PARAM_STR);
        $stmt->bindParam(3, $percentage, PDO::PARAM_STR);
        $stmt->bindParam(4, $bin_size, PDO::PARAM_INT);

        if($stmt->execute(array($user_id, $date, $percentage, $bin_size))) {
			return true;
		}
		else {
			return false;
		}
    }
    
    /**
	* GETs all user dates for a given user.
	* @return An array containing all of the results.
	*/
	public function getUserDates($user_id) {
		$stmt = $this->connection->prepare("SELECT * FROM user_date ud WHERE ud.user_id = ?");
        $stmt->bindParam(1, $user_id, PDO::PARAM_STR);
        
        $stmt->execute();
		$result = $stmt->fetchAll();
        
        return $result;
	}
	
	/* ------------------ COUNCIL METHODS ------------------ */
	/**
	* Creates a council in the database.
	* @return Returns the council ID as an integer if successful or null if unsuccessful.
	*/
	public function createCouncil($name, $address, $openinghours, $url, $phonenumber, $rssfeed) {
		$data_hash = md5(uniqid(rand(), true));
        
        $stmt = $this->connection->prepare("INSERT INTO council(name, address, openinghours, url, phonenumber, rssfeed, data_hash) VALUES(?, ?, ?, ?, ?, ?, ?)");
		$stmt->bindParam(1, $user_id, PDO::PARAM_STR);
		$stmt->bindParam(2, $address, PDO::PARAM_STR);
		$stmt->bindParam(3, $openinghours, PDO::PARAM_STR);
		$stmt->bindParam(4, $url, PDO::PARAM_STR);
        $stmt->bindParam(5, $phonenumber, PDO::PARAM_STR);
		$stmt->bindParam(6, $rssfeed, PDO::PARAM_STR);
        $stmt->bindParam(7, $data_hash, PDO::PARAM_STR);
		
		if($stmt->execute(array($name, $address, $openinghours, $url, $phonenumber, $rssfeed, $data_hash))) {
			return $this->connection->lastInsertId();
		}
		else {
			return NULL;
		}
	}
	
	/**
	* GETs all councils.
	* @return An array containing all of the results.
	*/
	public function getAllCouncils() {
		$stmt = $this->connection->prepare("SELECT * FROM council");
        $stmt->execute();
		$result = $stmt->fetchAll();
        
        return $result;
	}
	
	/**
	* GETs a single council with a given id.
	* @return A single row from the database.
	*/
	public function getCouncil($council_id) {
		$stmt = $this->connection->prepare("SELECT * FROM council c WHERE c.id = ?");
		$stmt->bindParam(1, $council_id, PDO::PARAM_INT);
		
		if ($stmt->execute(array($council_id))) {
			$result = $stmt->fetch();
			return $result;
		}
		else {
            return NULL;
        }
    }
    
    /**
	* GETs a single council and the area info with given ids.
	* @return A single row from the database.
	*/
	public function getCouncilArea($council_id, $area_id) {
		$stmt = $this->connection->prepare("SELECT c.id,
                                                c.name,
						c.address,
                                                c.openinghours,
                                                c.url,
                                                c.phonenumber,
                                                c.rssfeed,
                                                c.data_hash,
                                                ca.recycling_start,
                                                c.isBackToEarth,
                                                c.logo_filename,
                                                c.emailaddress,
                                                c.calendarSignedUp,
                                                c.recycle_bin_colour,
                                                c.greenwaste_bin_colour,
                                                c.garbage_link,
                                                c.recycling_link,
                                                c.dob_in_dump
                                                FROM `council` c
                                                LEFT JOIN `council_area` ca ON c.id=ca.council_id
                                                WHERE c.id = ? and ca.area_id = ?");
		$stmt->bindParam(1, $council_id, PDO::PARAM_INT);
        $stmt->bindParam(2, $area_id, PDO::PARAM_INT);
		
		if ($stmt->execute(array($council_id, $area_id))) {
			$result = $stmt->fetch();
			return $result;
		}
		else {
            return NULL;
        }
    }
    
    /* ------------------ EVENT METHODS ------------------ */
	/**
	* GETs all events for a given council_id/area_id pair
	* @return An array containing all of the results.
	*/
	public function getEvents($council_id, $area_id) {
        $stmt = $this->connection->prepare("SELECT et.id as type_id, e.start_date, e.duration
                                            FROM  `calendar_event_type` et
                                            LEFT JOIN  `calendar_event` e ON et.id = e.event_type_id
                                            WHERE e.council_id = ?
                                            AND e.area_id = ?");
        $stmt->bindParam(1, $council_id, PDO::PARAM_INT);
        $stmt->bindParam(2, $area_id, PDO::PARAM_INT);
        
        if ($stmt->execute(array($council_id, $area_id))) {
			$result = $stmt->fetchAll();
			return $result;
		}
		else {
            return NULL;
        }
        
		
	}
    
    /**
    * GETs all event types for a given council_id/area_id pair
	* @return An array containing all of the results.
	*/
	public function getEventTypes($council_id, $area_id) {
        $stmt = $this->connection->prepare("SELECT DISTINCT e.event_type_id as id, et.name, et.colour, et.link
                                            FROM `calendar_event` e
                                            LEFT JOIN `calendar_event_type` et ON e.event_type_id=et.id
                                            WHERE e.council_id = ?
                                            AND e.area_id = ?");
        $stmt->bindParam(1, $council_id, PDO::PARAM_INT);
        $stmt->bindParam(2, $area_id, PDO::PARAM_INT);
        
        if ($stmt->execute(array($council_id, $area_id))) {
			$result = $stmt->fetchAll();
			return $result;
		}
		else {
            return NULL;
        }
	}
    
    
	/* ------------------ BUSINESS METHODS ------------------ */	
	/**
	* Creates a business in the database.
	* @return Returns the business ID as an integer if successful or null if unsuccessful.
	*/
	public function createBusiness($name, $url, $address, $phone_number) {
		$stmt = $this->connection->prepare("INSERT INTO business(name, url, address, phone_number) VALUES(?, ?, ?, ?)");
		$stmt->bindParam(1, $name, PDO::PARAM_STR);
		$stmt->bindParam(2, $url, PDO::PARAM_STR);
		$stmt->bindParam(3, $address, PDO::PARAM_STR);
		$stmt->bindParam(4, $phone_number, PDO::PARAM_STR);
		
		if($stmt->execute(array($name, $url, $address, $phone_number))) {
			return $this->connection->lastInsertId();
		}
		else {
			return NULL;
		}
	}
	
	/**
	* GETs all businesses.
	* @return An array containing all of the results.
	*/
	public function getAllBusinesses() {
		$stmt = $this->connection->prepare("SELECT * FROM business");
        $stmt->execute();
		$result = $stmt->fetchAll();
        
        return $result;
	}
	
	/**
	* GETs a single business with a given id.
	* @return A single row from the database.
	*/
	public function getBusiness($business_id) {
		$stmt = $this->connection->prepare("SELECT * FROM business b WHERE b.id = ?");
		$stmt->bindParam(1, $business_id, PDO::PARAM_INT);
		
		if ($stmt->execute(array($business_id))) {
			$result = $stmt->fetch();
			return $result;
		}
		else {
            return NULL;
        }
    }

	/* ------------------ CATEGORY METHODS ------------------ */
	/**
	* Creates a category in the database.
	* @return Returns the category ID as an integer if successful or null if unsuccessful.
	*/
	public function createCategory($name) {
		$stmt =$this->connection->prepare("INSERT INTO category(name) VALUES(?)");
		$stmt->bindParam(1, $name, PDO::PARAM_STR);
		
		if($stmt->execute(array($name))) {
			return $this->connection->lastInsertId();
		}
		else {
			return NULL;
		}
	}
	
	/**
	* GETs all categories.
	* @return An array containing all of the results.
	*/
  public function getAllCategories() {
		$stmt = $this->connection->prepare("SELECT * FROM category");
        $stmt->execute();
		$result = $stmt->fetchAll();
        
        return $result;
	}

	/**
	* GETs a single category with a given id.
	* @return A single row from the database.
	*/
	public function getCategory($category_id) {
        $stmt = $this->connection->prepare("SELECT * FROM category c WHERE c.id = ?");
		$stmt->bindParam(1, $category_id, PDO::PARAM_INT);
		
		if ($stmt->execute(array($category_id))) {
			$result = $stmt->fetch();
			return $result;
		}
		else {
            return NULL;
        }
    }
	
	public function getItemsByCategory($category_id,$council_id) {
        
		$stmt = $this->connection->prepare("SELECT * FROM `item` WHERE `category_id` = " . $category_id ." AND council_id = ".$council_id);
		$stmt->execute();
		$result = $stmt->fetchAll();

        return $result;

    }
	
	

	/* ------------------ ITEM TAG METHODS ------------------ */
	/**
	* Creates an item tag in the database.
	* @return Returns the item tag ID as an integer if successful or null if unsuccessful.
	*/
	public function createItemTag($information) {
		$stmt = $this->connection->prepare("INSERT INTO item_tag(information) VALUES(?)");
		$stmt->bindParam(1, $information, PDO::PARAM_STR);
		
		if($stmt->execute(array($information))) {
			return $this->connection->lastInsertId();
		}
		else {
			return NULL;
		}
	}
	
	/**
	* GETs all item tags.
	* @return An array containing all of the results.
	*/
	public function getAllItemTags() {
		$stmt = $this->connection->prepare("SELECT * FROM item_tag");
        $stmt->execute();
		$result = $stmt->fetchAll();
        
        return $result;
	}

	/**
	* GETs a single item tag with a given id.
	* @return A single row from the database.
	*/
	public function getItemTag($item_tag_id) {
		$stmt = $this->connection->prepare("SELECT * FROM item_tag it WHERE it.id = ?");
		$stmt->bindParam(1, $item_tag_id, PDO::PARAM_INT);
		
		if ($stmt->execute(array($item_tag_id))) {
			$result = $stmt->fetch();
			return $result;
		}
		else {
            return NULL;
        }
    }

	/* ------------------ ITEM METHODS ------------------ */
	/**
	* Creates an item in the database.
	* @return Returns the item ID as an integer if successful or null if unsuccessful.
	*/
	public function createItem($council_id, $category_id, $name, $is_donate, $information, $tag_id, $business_id) {
		
		$stmt = $this->connection->prepare("INSERT INTO item(council_id, category_id, name, is_donate, information, tag_id, business_id) VALUES(?, ?, ?, ?, ?, ?, ?)");
		$stmt->bindParam(1, $council_id, PDO::PARAM_INT);
		$stmt->bindParam(2, $category_id, PDO::PARAM_INT);
		$stmt->bindParam(3, $name, PDO::PARAM_STR);
		$stmt->bindParam(4, $is_donate, PDO::PARAM_BOOL);
		$stmt->bindParam(5, $information, PDO::PARAM_STR);
		$stmt->bindParam(6, $tag_id, PDO::PARAM_INT);
		$stmt->bindParam(7, $business_id, PDO::PARAM_INT);
        
        //md5(uniqid(rand(), true));
		
		if($stmt->execute(array($council_id, $category_id, $name, $is_donate, $information, $tag_id, $business_id))) {
            
            //set a new data hash so the caching can be done
            $newmd5 = md5(uniqid(rand(), true));
            $md5stmt = $this->connection->prepare("UPDATE council SET data_hash = ? WHERE id = ?");
            $md5stmt->bindParam(1, $newmd5, PDO::PARAM_STR);
            $md5stmt->bindParam(2, $council_id, PDO::PARAM_INT);
                
            if(!$md5stmt->execute(array($newmd5, $council_id)))
                return NULL;
            
			return $this->connection->lastInsertId();
		}
		else {
			return NULL;
		}
	}
	
	/**
	* updates an item in the database.
	* @return Returns the item ID as an integer if successful or null if unsuccessful.
	*/
	public function updateItem($id, $tag_id, $information, $business_id, $is_recycle, $is_donate, $d_business_id, $d_information, $is_reuse, $ru_business_id, $ru_information) {
		
		$stmt = $this->connection->prepare("
			UPDATE 
				item 
			SET 
				tag_id = ?, 
				information = ?, 
				business_id = ?, 
				is_recycle = ?, 
				is_donate = ?, 
				d_business_id = ?, 
				d_information = ?, 
				is_reuse = ?, 
				ru_business_id = ?, 
				ru_information = ? 
			WHERE 
				id = ?");
		$stmt->bindParam(1, $tag_id, PDO::PARAM_INT);
		$stmt->bindParam(2, $information, PDO::PARAM_STR);
		$stmt->bindParam(3, $business_id, PDO::PARAM_INT);
		$stmt->bindParam(4, $is_recycle, PDO::PARAM_BOOL);
		$stmt->bindParam(5, $is_donate, PDO::PARAM_BOOL);
		$stmt->bindParam(6, $d_business_id, PDO::PARAM_INT);
		$stmt->bindParam(7, $d_information, PDO::PARAM_STR);
		$stmt->bindParam(8, $is_reuse, PDO::PARAM_BOOL);
		$stmt->bindParam(9, $ru_business_id, PDO::PARAM_INT);
		$stmt->bindParam(10, $ru_information, PDO::PARAM_STR);
		$stmt->bindParam(11, $id, PDO::PARAM_INT);
        

		if($stmt->execute(array($tag_id, $information, $business_id, $is_recycle, $is_donate, $d_business_id, $d_information, $is_reuse, $ru_business_id, $ru_information, $id))) {
			return $id;
		}else {
			return NULL;
		}
	}

	/**
	* GET SINGLE ITEM
	* @return Returns the single item with given id
	*/
	public function getItem($id){
	        $stmt = $this->connection->prepare("
		        SELECT 
				i.*, 
				it.information as item_tag, 
				b.name as business_name, 
				b.url as business_url, 
				b.address as business_address, 
				b.phone_number as business_phone_number, 
				ct.icon_filename as icon_filename, 
				dbus.name as d_business_name, 
				dbus.url as d_business_url, 
				dbus.address as d_business_address, 
				dbus.phone_number as d_business_phone_number, 
				rubus.name as ru_business_name, 
				rubus.url as ru_business_url, 
				rubus.address as ru_business_address, 
				rubus.phone_number as ru_business_phone_number 
			FROM 
				`item` i 
				join `item_tag` it on i.tag_id = it.ID 
				LEFT JOIN council c on c.id = i.council_id 
				LEFT JOIN business b on b.id = i.business_id 
				LEFT JOIN category ct on ct.id = i.category_id 
				LEFT JOIN business dbus on dbus.id = i.d_business_id 
				LEFT JOIN business rubus on rubus.id = i.ru_business_id 
			WHERE 
				i.id = ?");
		$stmt->bindParam(1, $id, PDO::PARAM_INT);
		
		if ($stmt->execute(array($id))) {
			$result = $stmt->fetch();
			return $result;
		}else {
            		return NULL;
       		}
	}


	/**
	* Searches for all items with a given council that matches a search term.
	* @return An array containing all of the results.
	*/
	public function searchForItemsWithItemTag($council_id, $search_term) {
	
		//Selects all items that have a given search term in their item_tag
		$stmt = $this->connection->prepare("
			SELECT 
				i.*, 
				it.information as item_tag, 
				b.name as business_name, 
				b.url as business_url, 
				b.address as business_address, 
				b.phone_number as business_phone_number, 
				ct.icon_filename as icon_filename, 
				dbus.name as d_business_name, 
				dbus.url as d_business_url, 
				dbus.address as d_business_address, 
				dbus.phone_number as d_business_phone_number, 
				rubus.name as ru_business_name, 
				rubus.url as ru_business_url, 
				rubus.address as ru_business_address, 
				rubus.phone_number as ru_business_phone_number 
			FROM 
				`item` i 
				join `item_tag` it on i.tag_id = it.ID 
				LEFT JOIN council c on c.id = i.council_id 
				LEFT JOIN business b on b.id = i.business_id 
				LEFT JOIN category ct on ct.id = i.category_id 
				LEFT JOIN business dbus on dbus.id = i.d_business_id 
				LEFT JOIN business rubus on rubus.id = i.ru_business_id 
			WHERE c.id = ?
			AND MATCH (it.information) AGAINST (? IN BOOLEAN MODE)");
		
		$stmt->bindParam(1, $council_id, PDO::PARAM_INT);
		$stmt->bindParam(2, $search_term, PDO::PARAM_STR);
		
		$stmt->execute();
		$result = $stmt->fetchAll();
		
		return $result;
	}
	
	/**
	* Returns for all items with a given council.
	* @return An array containing all of the results.
	*/
	public function searchForItemsWithCouncil($council_id) {	
		//Selects all items that have a given council
		

		$stmt = $this->connection->prepare("
			SELECT 
				i.*, 
				it.information as item_tag, 
				b.name as business_name, 
				b.url as business_url, 
				b.address as business_address, 
				b.phone_number as business_phone_number, 
				ct.icon_filename as icon_filename, 
				dbus.name as d_business_name, 
				dbus.url as d_business_url, 
				dbus.address as d_business_address, 
				dbus.phone_number as d_business_phone_number, 
				rubus.name as ru_business_name, 
				rubus.url as ru_business_url, 
				rubus.address as ru_business_address, 
				rubus.phone_number as ru_business_phone_number 
			FROM 
				`item` i 
				join `item_tag` it on i.tag_id = it.ID 
				LEFT JOIN council c on c.id = i.council_id 
				LEFT JOIN business b on b.id = i.business_id 
				LEFT JOIN category ct on ct.id = i.category_id 
				LEFT JOIN business dbus on dbus.id = i.d_business_id 
				LEFT JOIN business rubus on rubus.id = i.ru_business_id 
			WHERE 
			        c.id = ? ");
       
		$stmt->bindParam(1, $council_id, PDO::PARAM_INT);
		
		$stmt->execute();
		$result = $stmt->fetchAll();
/*
			$rowCount = $stmt->rowCount();		
			$response = array();
			$response["count"]=$rowCount;
			$response["result"]=array();
			for($i=0; $i<$rowCount; $i++){
				$row = $stmt->fetch();
				array_push($response["result"], $row);
			}
*/				

		return $result;
	}


}

?>