<?php

/**
 * This class creates a connection to the Sustain Me database.
 *
 * @author     Alex Portlock (alex.portlock@sustainme.com.au)
 * @copyright  2015 The Sustain Me Group
 */
class DatabaseConnection {

    private $connection;

    function __construct() {        
    }

    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {
		
        include_once dirname(__FILE__) . '/Config.php';
		
		try {		
			$this->connection = new PDO('mysql:host=localhost;dbname=sustainm_SustainMeDB', DB_USERNAME, DB_PASSWORD);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			if (mysqli_connect_errno()) {
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}

			return $this->connection;
		} catch (PDOException $e) {		
			echo $e->getMessage();
			die();
		}
    
	}

}

?>